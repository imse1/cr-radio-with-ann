import adi
import numpy as np
import time
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
from scipy.signal import butter,lfilter,filtfilt

def write_data(file_path, data):
    with open(file_path+'data8.txt', 'w') as outfile:
        # I'm writing a header here just for the sake of readability
        # Any line starting with "#" will be ignored by numpy.loadtxt
        outfile.write('# Array shape: {0}\n'.format(data.shape))
        
        # Writing the entire 2D array to the file
        np.savetxt(outfile, data, fmt='%-7.2f')

        # Writing out a break to indicate the end of data
        outfile.write('# End of data\n')


def compute_envelope(signal, num_points):
    x = np.arange(len(signal))
    cs = CubicSpline(x, signal)
    new_x = np.linspace(0, len(signal) - 1, num_points)
    return cs(new_x)

def local_maximum(data,n):
    output=np.empty_like(data)
    output[:,:n]=data[:,:n]
    for i in range (len(data)):
        for j in range (n,len(data[0])):
            comparaison=data[i,j-n:j+1]
            output[i,j]=max(comparaison)
    return(output)            

def butter_lowpass_filter(data, cutoff, fs, order):
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

########################### INITIALISATION ###################################
VERBOSE=0
output_per_run=1000
# SDR parameters
sample_rate = 60e6  # Hz CORRESPOND TO OUTPUT DATA RATE
PRECISION=5
num_samples = 1024*PRECISION  # Number of samples to read in each iteration
gain = 20  # dB
num_runs = 1


# Set PlutoSDR parameters for Wi-Fi channel scanning
center_freqs = np.array([2.412e9, 2.417e9, 2.422e9, 2.427e9, 2.432e9, 2.437e9,
                            2.442e9, 2.447e9, 2.452e9, 2.457e9, 2.462e9, 2.467e9, 2.472e9, 2.484e9])  # List of center frequencies for channels 1 to 5
CENTER=(center_freqs[0]+center_freqs[-1])/2
NUM_CHANNEL=len(center_freqs)


# Connect to the PlutoSDR device
sdr = adi.Pluto('ip:192.168.50.85')
sdr.sample_rate = int(sample_rate)
sdr.rx_buffer_size = num_samples  # this is the buffer the Pluto uses to buffer samples
sdr.gain_control_mode_chan0 = 'manual'
sdr.rx_hardwaregain_chan0 = gain  # dB. Allowable range is 0 to 74.5 dB
bandwidth = 70e6  # Bandwidth in Hz (20 MHz for Wi-Fi channels)
sdr.rx_rf_bandwidth = int(bandwidth)
sdr.rx_lo = int(CENTER)
##############################################################################




channel_occupancy=np.empty((NUM_CHANNEL,output_per_run*num_runs))
fftfreqs = np.fft.fftfreq(num_samples, 1/sample_rate)+CENTER
NFREQ=len(fftfreqs)
idx = np.argsort(fftfreqs)
deltaf=fftfreqs[1]-fftfreqs[0]
fftfreqs=fftfreqs[idx]
start_time=time.time()
for i in range(output_per_run*num_runs):

    # Receive samples from PlutoSDR
    data = sdr.rx()  # 'data' will contain the received samples in a complex array
    #ps = np.abs(np.fft.fft(data))**2
    module=np.abs(data)
    ps = np.fft.fft(module)
    ps=np.abs(ps[idx])
    flog=np.log10(ps)
    flog=flog[idx]
    BP=np.empty((NUM_CHANNEL))
    for j in range(NUM_CHANNEL):
        BP[j]=np.multiply(np.sum(flog[j*NFREQ//NUM_CHANNEL:(j+1)*NFREQ//NUM_CHANNEL]),NFREQ//NUM_CHANNEL)
    
    channel_occupancy[:,i]=BP

total_time=time.time()-start_time
print("time to gather data : ",total_time,"s")

print('Computing envelope and filtering')
start_time3 = time.time()
N=4
channel_envelopes=np.empty((NUM_CHANNEL,output_per_run*num_runs*N))

T = total_time/(output_per_run*num_runs*N)         # Sample Period
fs = 1/T       # sample rate, Hz
cutoff = 2      # desired cutoff frequency of the filter, Hz ,      slightly higher than actual 1.2 Hz
nyq = 0.5 * fs  # Nyquist Frequencyorder = 2       # sin wave can be approx represented as quadratic
normal_cutoff = cutoff / nyq
fOrder=2
# Design the Butterworth low-pass filter
b, a = butter(fOrder, normal_cutoff, btype='low')
for x in range(0, NUM_CHANNEL):
    #avg=average(channel_occupancy[x],20)
    channel_envelopes[x]=compute_envelope(channel_occupancy[x],output_per_run*num_runs*N)
    #channel_envelopes[x]=savgol_filter(channel_envelopes[x], 8, 3)
    #moy=np.median(channel_occupancy[x])
    channel_envelopes[x]=filtfilt(b, a, channel_envelopes[x], padlen=fOrder*3)
end_time3 = time.time()
print('****** Envelope computed ****** --> seconds elapsed:', end_time3 - start_time3)



time_array=np.linspace(0,total_time,output_per_run*num_runs)
time_array2=np.linspace(0,total_time,output_per_run*num_runs*N)
max_value2=np.max(channel_envelopes)
min_value2=np.min(channel_envelopes)
print("min : ",min_value2, "   max : ",max_value2)
min_value2=250000
max_value2=400000
for i in range(len(channel_occupancy)):
    channel_occupancy[i]=100*(channel_occupancy[i]-min_value2)/(max_value2-min_value2)
    channel_envelopes[i]=100*(channel_envelopes[i]-min_value2)/(max_value2-min_value2)
    #channel_occupancy[i]=np.clip(channel_occupancy[i], 0, 100)
    #channel_envelopes[i]=np.clip(channel_envelopes[i], 0, 100)
    plt.plot(time_array,channel_occupancy[i],'.-',color='b')
    plt.plot(time_array2,channel_envelopes[i],'.-',color='r')
    plt.xlabel('time (s)')
    plt.ylabel('occupancy (%)')
    plt.title('Power throught time channel '+str(i+1))
    plt.show()

write_data("real_data/",channel_envelopes)