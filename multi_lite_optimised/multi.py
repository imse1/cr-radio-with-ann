import os
import random
import time
import statistics
import keras.metrics
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
from keras.utils.vis_utils import plot_model
from tensorflow import keras
import tensorflow as tf
import tensorflow_model_optimization as tfmot
from keras import layers
from keras.models import model_from_json
import keras.backend as k

def naive_prediction(input_data, n_output):
    input=[]
    for i in range (len(input_data[0])):
        input.append(input_data[0][i][0])
    input_data=input
    out=[]
    avg=np.average(input_data)
    for i in range(n_output):
        out.append(avg)
    return(out)

    # Convert input data to numpy array
    #X = np.array(input_data).reshape(-1, 1)

    # Fit a linear regression model
    #reg = LinearRegression()
    #reg.fit(X, np.arange(len(input_data)))

    # Generate the continuity of the input data
    #output_data = reg.predict(X).tolist()
    #for i in range(len(input_data), len(input_data) + n_output):
    #    output_data.append(reg.predict([[i]])[0])
    
    #return output_data[len(X):]


def find_input_output_order(interpreter,n_channel):
    mapping = [{'input_index': None, 'output_index': None} for _ in range(n_channel)]
    signatures = interpreter.get_signature_list()
    #print("signatures : ",signatures)
    input_details=interpreter.get_input_details()
    output_details=interpreter.get_output_details()
    min=9999
    for i in range(len(output_details)):
        if output_details[i]['index']<min:
            min=output_details[i]['index']
    for i in range(n_channel):
        q=0
        while(signatures['serving_default']['inputs'][i]!=input_details[q]['name'][-12:-2]):
            q=q+1
        mapping[i]={'input_index':input_details[q]['index'] , 'output_index':min+i}
        
    #print("mapping : ",mapping)
    return mapping

def print_tflite_model_info(tflite_model_path):
    # Load the TFLite model
    interpreter = tf.lite.Interpreter(model_path=tflite_model_path)
    interpreter.allocate_tensors()

    # Get input details
    input_details = interpreter.get_input_details()
    print("Input details:")
    for input_tensor in input_details:
        print(f"Name: {input_tensor['name']}")
        print(f"Shape: {input_tensor['shape']}")
        print(f"Data Type: {input_tensor['dtype']}")
        print(f"Index: {input_tensor['index']}")
        print("")

    # Get output details
    output_details = interpreter.get_output_details()
    print("Output details:")
    for output_tensor in output_details:
        print(f"Name: {output_tensor['name']}")
        print(f"Shape: {output_tensor['shape']}")
        print(f"Data Type: {output_tensor['dtype']}")
        print(f"Index: {output_tensor['index']}")
        print("")

def convert_to_float32(data):
    #print("shape input data : ",np.shape(data))
    full_data = np.empty_like(data, dtype=np.float32)
    for i in range(len(data)):
        for j in range(len(data[i][0])):
            # Convert each array to float32 and assign directly to the empty array
            full_data[i][0][j][0] = data[i][0][j][0].astype(np.float32)
    #print("shape output data : ",np.shape(full_data))
    return full_data

def convert_to_tflite(keras_model, tflite_model_path):
    print('----> Start of conversion to TFLite <----')
    # Convert the Keras model to a TensorFlow Lite model
    converter = tf.lite.TFLiteConverter.from_keras_model(keras_model)
    

    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model = converter.convert()

    # Save the TensorFlow Lite model to a file
    with open(tflite_model_path, 'wb') as f:
        f.write(tflite_model)
    print('----> End of conversion <----')
    print(f"TensorFlow Lite model saved to: {tflite_model_path}")
    return(tflite_model_path)

def make_prediction(history, n_input, interpreter, n_output,mapping):
    n_channel = len(history)
    # Flatten data
    data = np.array(history)
    # List of 'n' inputs
    input_all = [np.zeros((1, n_input, 1)) for _ in range(n_channel)]

    for w in range(n_channel):
        data_sing = data[w].reshape(data[w].shape[0] * data[w].shape[1], 1)
        # Retrieve last observations for input data
        input_x = data_sing[-n_input:, 0]
        # Reshape into [1, n_input, 1]
        input_all[w] = np.array(input_x.reshape((1, len(input_x), 1)))

    input_all = convert_to_float32(input_all)
    output_tensor = np.empty((n_channel, 1, n_output))
    for i in range(n_channel):
        input_index=mapping[i]['input_index']
        input_data_channel = input_all[i]
        # Set input tensor for the current channel
        interpreter.set_tensor(input_index, input_data_channel)


    interpreter.invoke()

    for i in range(n_channel):
        output_index=mapping[i]['output_index']
        # Get the prediction for the current channel
        output_tensor_channel = interpreter.get_tensor(output_index)
        # Append the prediction to the output_tensor list for the current channel
        output_tensor[i]=output_tensor_channel
    return output_tensor

# split a univariate dataset into train / test sets

def split_dataset(data, train_percent):
    limit_train = int((train_percent / 100) * len(data[0]))
    
    train_all = np.empty((len(data), limit_train))
    test_all = np.empty((len(data), len(data[0]) - limit_train))

    for i in range(len(data)):
        train_all[i] = data[i][:limit_train]
        test_all[i] = data[i][limit_train:]

    return train_all, test_all

def evaluate_model_cnn(train, test, n_input, n_output, folder_path, name_model, load_model, model_number):
    print("shape of test data :",np.shape(test))
    train_runtime = 0
    load_runtime = 0
    n_channels = len(train)
    # fit model
    if not load_model:
        model, train_runtime = build_model_cnn(
            train, n_input, n_output, folder_path, name_model)
    else:
        load_start = time.time()
        # load model
        model = loadhd5(model_number)

        print('Load network runtime --> seconds elapsed:', time.time()-load_start)
    print(folder_path)
    tflite_model_path=convert_to_tflite(model,folder_path+"tflite_model")
    #print_tflite_model_info(tflite_model_path)
    # history is a list of n_input samples
    history_all = []
    test_all = []
    for m in range(0, n_channels):
        # reshape data
        train_sing = train[m].reshape(len(train[m]), 1)
        test_sing = test[m].reshape(len(test[m]), 1)

        # reshape data
        limit_train = int(len(train[m]) / n_output)
        limit_test = int(len(test[m]) / n_output)
        train_sing = train_sing[0:limit_train * n_output]
        test_sing = test_sing[0:limit_test * n_output]
        
        # restructure into windows of sequence data
        train_sing = np.array(np.split(train_sing, int(len(train[m]) / n_output)))
        test_sing = np.array(np.split(test_sing, int(len(test[m]) / n_output)))

        history_all.append(list(train_sing))
        # append test set of individual channels
        test_all.append(test_sing)
    # Load the TFLite model
    interpreter = tf.lite.Interpreter(model_path=tflite_model_path)
    interpreter.allocate_tensors()
    mapping=find_input_output_order(interpreter,n_channels)
    #print_tflite_model_info(tflite_model_path)

    # walk - forward validation over each n_input samples
    predictions = np.empty((len(test_all[0]),n_channels,1,n_output))
    forecast_times = np.empty(len(test_all[0])+1)
    
    forecast_start = time.time()
    forecast_times[0]=forecast_start
    print('----> Forecasting <----')
    for i in range(len(test_all[0])):
        
        forecast_times[i+1] =time.time()
        # store the predictions
        #predictions[i]=yhat_sequence
        predictions[i]=make_prediction(history_all,n_input,interpreter,n_output,mapping)
        # get real observation and add to history for predicting the next samples
        for j in range(0, n_channels):
            history_all[j].append(test_all[j][i, :])
    forecast_end = time.time()
    forecast_runtime = forecast_end - forecast_start
    print('Forecast runtime --> seconds elapsed:', forecast_runtime)
    print("shape history all : ",np.shape(history_all))
    avg_forecast_times = forecast_runtime/len(test_all[0])
    
    rmse_score = np.empty((n_channels))
    rmse_scores = np.empty((n_channels,n_output))
    for m in range(0, n_channels):
        # evaluate predictions of each sample
        score, scores = evaluate_forecasts(
            test_all[m][:, :, 0], predictions[:, m, 0, :])
        rmse_score[m]=score
        rmse_scores[m]=scores


    predictions_list = list()
    original_list = list()
    for n in range(0, n_channels):
        # reshape the arrays into an easily readable format
        predictions_sing = predictions[:, n, 0, :].reshape((predictions.shape[0] * predictions.shape[3],
                                                            test_all[0].shape[2]))
        predictions_list.append(predictions_sing)
        original = test_all[n][:, :, 0]
        original = original.reshape(
            (original.shape[0] * original.shape[1], test_all[0].shape[2]))
        original_list.append(original)


    return rmse_score, rmse_scores, original_list, predictions_list, forecast_runtime, train_runtime, load_runtime, \
        avg_forecast_times


# train the model
def build_model_cnn(train, n_input, n_output, f_path, model_name):
    n_channels = len(train)

    # define parameters
    verbose, epochs, batch_size = 2, 20, 160

    model_output = np.empty(n_channels, dtype=object)
    model_input = np.empty(n_channels, dtype=object)
    train_x_all = np.empty(n_channels, dtype=object)
    train_y_all = np.empty(n_channels, dtype=object)

    for j in range(0, n_channels):
        # reshape data
        train_sing = train[j].reshape(len(train[j]), 1)
        limit = int(len(train[j]) / n_output)
        train_sing = train_sing[0:limit*n_output]
        # restructure into windows of sequence data
        train_sing = np.array(np.split(train_sing, int(len(train[j]) / n_output)))

        # prepare data
        train_x, train_y = to_supervised(train_sing, n_input, n_output)
        train_x_all[j] = train_x
        train_y_all[j] = train_y

    train_x = train_x_all
    train_y = train_y_all

    n_timesteps, n_features, n_outputs = train_x[0].shape[1], train_x[0].shape[2], train_y[0].shape[1]
    letters=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    for q in range(0, n_channels):
        name_channel = 'channel_' + letters[q//26] + letters[q%26]
        # define input node
        channel_input = keras.Input(
            shape=(n_timesteps, n_features), name=name_channel)
        model_input[q] = channel_input

    # Convert array to list
    input_list = model_input.tolist()
    
    layer_1_1=[]
    layer_1_2=[]
    layer_1_3=[]
    layer_1_5=[]
    for i in range(n_channels):
        layer_1_1.append(layers.Conv1D(filters=64, kernel_size=3,activation='relu')(input_list[i]))
        # adding a second hidden layer
        layer_1_2.append(layers.MaxPooling1D(pool_size=2)(layer_1_1[i]))
        # adding a third hidden layer
        layer_1_3.append(layers.Flatten()(layer_1_2[i]))
    # adding a fourth hidden layer
    #layer_1_4 = layers.Concatenate()(layer_1_3)
    # adding a fifth hidden layer
        #layer_1_5.append(tfmot.quantization.keras.quantize_annotate_layer(layers.Dense(n_output*3, activation='relu'))(layer_1_3[i]))
        layer_1_5.append(layers.Dense(n_output*3, activation='relu')(layer_1_3[i]))
    #layer_1_5 = layers.Dense(n_output*n_channels*4, activation='relu')(layer_1_3)

        name_channel = 'out_channel_' + letters[i//26] + letters[i%26]
        # adding the output layer
        output_1 = layers.Dense(
            n_outputs, activation='linear', name=name_channel)(layer_1_5[i])    
        model_output[i] = output_1

    # **************************************** Instantiate Input and Outputs ***************************************** #
    # Convert arrays to list
    output_list = model_output.tolist()
    train_x_all = train_x_all.tolist()
    train_y_all = train_y_all.tolist()

    model = keras.Model(inputs=input_list,
                        outputs=output_list, name='channels_models')

    # ******************************************** Compiling both inputs ********************************************* #

    # compiling the neural network
    model.compile(loss='mse', optimizer='adam', metrics=['acc', 'mse'])


    # Plot and save network model
    plot_model(model, show_shapes=True, to_file=f_path +
               model_name + '_network_model.png', dpi=300)

    # fit network
    print('----> Training network <----')
    train_start = time.time()
    model.fit(train_x_all, train_y_all, epochs=epochs,
              batch_size=batch_size, verbose=verbose)
    train_end = time.time()
    train_runtime = train_end - train_start
    print('Training runtime --> seconds elapsed:', train_runtime)

    savehd5(model, f_path)

    return model, train_runtime


# convert history into inputs and outputs
def to_supervised(train, n_input, n_output):
    # flatten data
    data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
    x, y = list(), list()
    in_start = 0
    # step over the entire history one time step at a time
    for _ in range(len(data)):
        # define the end of the input sequence
        in_end = in_start + n_input
        out_end = in_end + n_output
        # ensure we have enough data for this instance
        if out_end < len(data):
            x_input = data[in_start:in_end, 0]
            x_input = x_input.reshape((len(x_input), 1))
            x.append(x_input)
            y.append(data[in_end:out_end, 0])
        # move along one time step
        in_start += 1
    return np.array(x), np.array(y)


# make a forecast
def forecast(model, history, n_input):
    n_channel=len(history)
    # flatten data
    data = np.array(history)
    # list of 'n' inputs
    input_all = [np.zeros((1,n_input,1)) for i in range (n_channel)]
    for w in range(0, n_channel):
        data_sing = data[w].reshape(data[w].shape[0]*data[w].shape[1], 1)
        # retrieve last observations for input data
        input_x = data_sing[-n_input:, 0]
        # reshape into [1, n_input, 1]

        input_all[w]=np.array(input_x.reshape((1, len(input_x), 1)))
    yhat = model(input_all, training=False)

    return yhat


# evaluate one or more weekly forecasts against expected values
def evaluate_forecasts(actual, predicted):
    scores = list()
    # calculate an RMSE score for each sample
    for i in range(actual.shape[1]):
        # calculate mse
        mse = mean_squared_error(actual[:, i], predicted[:, i])
        # calculate rmse
        rmse = np.sqrt(mse)
        # store
        scores.append(rmse)
    # calculate overall RMSE
    s = 0
    for row in range(actual.shape[0]):
        for col in range(actual.shape[1]):
            s += (actual[row, col] - predicted[row, col]) ** 2
    score = '%.3f' % (np.sqrt(s/(actual.shape[0] * actual.shape[1])))
    return score, scores


def multi_plots(original, predictions, name_model,model_number, folder_path, score, n_channel):

    name_model=name_model[:-2]+str(model_number)
    for i in range(0, n_channel):
        if i<10:
            name='0'+str(i)
        else :
            name=str(i)

        # Setting the figure
        fig = plt.figure()
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)

        ax1.plot(original[i])
        ax2.plot(predictions[i])

        ax1.set_title('- Original -', fontsize=10)
        ax2.set_title('- Predicted -- ' + str(score[i]), fontsize=10)

        fig.tight_layout()

        # Saving the plot as an image
        fig.savefig(folder_path + 'original_vs_predicted_' + str(name_model) + str(name) + '.png',
                    bbox_inches='tight', dpi=300)

        plt.clf()
        plt.close()


def normalize(data):
    mu = statistics.mean(data)
    sigma = statistics.stdev(data, mu)
    standardized_train = (data - mu) / sigma

    return standardized_train


def savehd5(model, folder_path):
    # serialize model to JSON
    model_json = model.to_json()
    with open(folder_path + "model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(folder_path + "model.h5")
    print("Saved model to disk")


def loadhd5(model_number):
    # load json and create model
    path_model=folder_path[:-3] + str(model_number) + '/'
    json_file = open(path_model + 'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(path_model + "model.h5")
    
    #converter = tf.lite.TFLiteConverter.from_saved_model(path_model + "model.h5")
    #converter.optimizations = [tf.lite.Optimize.DEFAULT]
    #tflite_quant_model = converter.convert()
    
    print("----> Loaded model from disk <----")
    return loaded_model
    #return tflite_quant_model


def evalhd5(loaded_model, x, y):
    # evaluate loaded model on test data
    loaded_model.compile(loss='binary_crossentropy',
                         optimizer='rmsprop', metrics=['accuracy'])
    score = loaded_model.evaluate(x, y, verbose=0)
    print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1] * 100))


def create_sequence(length_sequence,number_of_level,number_of_channel):
    output_sequence=[]
    for j in range (number_of_channel):
        newlist=[]
        for i in range (length_sequence):
            newlist.append(random.randint(0, number_of_level))
        output_sequence.append(newlist)
    return(output_sequence)

def generate_data_input(multilevel_sequence, length_symbol, length_transition, noise_prop):
    
    dataset = []
    max_seq = max(multilevel_sequence)
    for i in range(len(multilevel_sequence)):
        for j in range(length_symbol):
            value = random.gauss(multilevel_sequence[i], max_seq*noise_prop/4)/max_seq
            #if (value > 1):        
            #    value = 1
            #elif (value < 0):
            #    value = 0
            dataset.append(value)
        if (i != (len(multilevel_sequence)-1)):
            for j in range(length_transition):
                value = random.gauss(multilevel_sequence[i]-(
                    (multilevel_sequence[i]-multilevel_sequence[i+1])*j/length_transition), max_seq*noise_prop)/max_seq #we have "legth_transion" values between 2 sequence's data
            #   if (value > 1):                                                                                 #we are then drawing a line between these 2 values with some noise
            #        value = 1
            #    elif (value < 0):
            #        value = 0
                dataset.append(value)
    return (dataset)   

def decision(prediction,max_ampl,delay,trigger):
    output=[]
    last_channel=0
    new_channel=0
    number_above=0
    for i in range (len(prediction[0])):
        if(prediction[last_channel][i]>trigger*max_ampl):
            value_channels=[prediction[j][i] for j in range(len(prediction))]
            max_channel=np.array(value_channels).argmin()
            if(max_channel!=last_channel and (prediction[last_channel][i]-prediction[max_channel][i])>0.2):
                if(max_channel==new_channel):
                    number_above=number_above+1
                else:
                    new_channel=max_channel
                    number_above=0
                if(number_above>delay):    
                    last_channel=max_channel
                    number_above=0
        else:
            delay=0
        output.append(last_channel)
    return(output)

def plotting(test,decideur,name_model):
    colors = ['red', 'green', 'blue', 'cyan', 'magenta', 'yellow', 'black', 'white', 'orange', 'purple']
    # Create a figure and subplots
    if (len(test)>4):
        nbr_plot=4
    else:
        nbr_plot=len(test)

    fig, axs = plt.subplots(nbr_plot+1, 1, figsize=(12, 6))

    for i in range (nbr_plot):
        axs[i].plot(test[i],color = colors[i])
        axs[i].set_title('Channel ' + str(i))

    # Plot the fifth graph
    axs[nbr_plot].plot(decideur)
    axs[nbr_plot].set_title('Decideur')

    # Adjust spacing between subplots
    plt.tight_layout()

    # Saving the plot as an image
    fig.savefig(folder_path + 'decision_bolck' + str(name_model) + '.png',
                bbox_inches='tight', dpi=300)

def read_data(file_path):
    # Read the array from disk
    new_data = np.loadtxt(file_path+'data.txt')
    #new_data=np.reshape(new_data,(n_channel))
    print("shape loaded data : ",np.shape(new_data))
    return(new_data.astype(np.float32))

if __name__ == '__main__':


    SIMULATION_NUMBER=28
    num_channels=14
    train_percentage = 98
    # define the total samples to use as input and number of output samples to be predicted.
    num_input = 20
    num_output = 30


    # +++++++++++++++++++++++++++++++++++++++++++++ FOLDER PATH TO SAVE DATA +++++++++++++++++++++++++++++++++++++++++ #
    folder_path='data/simulation'+str(SIMULATION_NUMBER)+'/'

    os.makedirs(folder_path)

    # +++++++++++++++++++++++++++++++++++++++++++++++ DATASET GENERATION +++++++++++++++++++++++++++++++++++++++++++++ #


    all_datasets = []
    
    print("----> Starting Data generation <----")
    all_datasets=read_data("")
    print("data shape : ", np.shape(all_datasets))
    print("----> End of data generation <----")



    load_model = False
    name_model = 'test_model'+str(SIMULATION_NUMBER)
    model_number = 18
    if (load_model==False):
        model_number=SIMULATION_NUMBER
    else:
        train_percentage=5
    # split into train and test
    train_set, test_set = split_dataset(all_datasets, train_percentage)
    # train and predict
    score, scores, original, predictions, forecast_runtime, train_runtime, load_runtime, avg_forecast_times = \
        evaluate_model_cnn(train_set, test_set, num_input,
                        num_output, folder_path, name_model, load_model, model_number)

    #plot original and predicted dataset and save graphical comparison to image file.
    multi_plots(original, predictions, name_model,model_number,
                folder_path, score, num_channels)
    

    
    print("Average forecast time for a single prediction : ",int(avg_forecast_times*1000000)/1000 , "ms")
    amplitude_max=1
    delay=3
    trigger=0.05
    decideur=decision(predictions,amplitude_max,delay,trigger)
    plotting(predictions,decideur,name_model)


