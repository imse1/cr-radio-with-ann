import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import random
import adi
from scipy.interpolate import CubicSpline
from scipy.signal import butter,filtfilt

def print_tflite_model_info(interpreter):
    # Get input details
    input_details = interpreter.get_input_details()
    print("Input details:")
    for input_tensor in input_details:
        print(f"Name: {input_tensor['name']}")
        print(f"Shape: {input_tensor['shape']}")
        print(f"Data Type: {input_tensor['dtype']}")
        print(f"Index: {input_tensor['index']}")
        print("")

    # Get output details
    output_details = interpreter.get_output_details()
    print("Output details:")
    for output_tensor in output_details:
        print(f"Name: {output_tensor['name']}")
        print(f"Shape: {output_tensor['shape']}")
        print(f"Data Type: {output_tensor['dtype']}")
        print(f"Index: {output_tensor['index']}")
        print("")

def create_signature(n_channel):
    alpha=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    signature=[]
    for i in range(n_channel):
        signature.append(alpha[i//26]+alpha[i%26])
    return(signature)


def find_input_output_order(interpreter,n_channel):
    # Initialize a list of dictionaries to store input and output indices for each channel
    mapping = [{'input_index': None, 'output_index': None} for _ in range(n_channel)]

    # Create signatures for the channels
    signatures = create_signature(n_channel)

    # Get input and output details from the interpreter
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Find the minimum index among output details
    min_index = min(output_details, key=lambda x: x['index'])['index']

    # Iterate through each output detail and find the minimum index
    for i in range(n_channel):
        q = 0

        # Find the matching signature in input details
        while signatures[i] != input_details[q]['name'][-4:-2]:
            q = q + 1

        # Store the input and output indices in the mapping
        mapping[i] = {'input_index': input_details[q]['index'], 'output_index': min_index + i}

    return mapping

def load_tflite_model(tflite_model_path):
    interpreter = tf.lite.Interpreter(model_path=tflite_model_path)
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    
    model_info = {
        'n_channel': len(input_details),
        'n_input': input_details[0]['shape'][1],
        'n_output': output_details[0]['shape'][1]
    }
    
    n_channels = len(input_details)
    mapping = find_input_output_order(interpreter, n_channels)  # Assuming find_input_output_order is defined
    
    return interpreter, mapping, model_info

def convert_to_tflite(keras_model, tflite_model_path):
    # Convert the Keras model to a TensorFlow Lite model
    converter = tf.lite.TFLiteConverter.from_keras_model(keras_model)
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model = converter.convert()
    
    # Save the TensorFlow Lite model to a file
    with open(tflite_model_path, 'wb') as f:
        f.write(tflite_model)
    
    # Initialize interpreter and extract input/output details
    interpreter = tf.lite.Interpreter(model_path=tflite_model_path)
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    
    # Determine model information
    model_info = {
        'n_channel': len(input_details),
        'n_input': input_details[0]['shape'][1],
        'n_output': output_details[0]['shape'][1]
    }
    
    n_channels = len(input_details)
    mapping = find_input_output_order(interpreter, n_channels)  # Assuming find_input_output_order is defined
    
    return interpreter, mapping, model_info

def make_prediction(input_data, n_input, interpreter, n_output,mapping):
    n_channel = len(input_data)
    output_tensor = np.empty((n_channel, 1, n_output))
    input_data=np.reshape(input_data,(n_channel,1,n_input,1))
    
    for i in range(n_channel):
        # Set input tensor for the current channel
        interpreter.set_tensor(mapping[i]['input_index'], input_data[i].astype(np.float32))
    #run model
    interpreter.invoke()

    for i in range(n_channel):
        # Get the prediction for the current channel
        output_tensor_channel = interpreter.get_tensor(mapping[i]['output_index'])
        # Append the prediction to the output_tensor list for the current channel
        output_tensor[i]=output_tensor_channel
    
    output_tensor=np.reshape(output_tensor,(n_channel,n_output))
    return output_tensor


def create_sequence(length_sequence,number_of_level,number_of_channel):
    output_sequence = []  # List to store the generated multilevel sequences
    
    for j in range(number_of_channel):
        newlist = []  # List to store a single multilevel sequence
        
        for i in range(length_sequence):
            newlist.append(random.randint(0, number_of_level))  # Generate random multilevel values
        
        output_sequence.append(newlist)  # Append the generated sequence to the output list
    
    return output_sequence  # Return the list of generated multilevel sequences

def generate_data_input(multilevel_sequence, length_symbol, length_transition, noise_prop):   
    dataset = []  # List to store the generated data input
    max_seq = max(multilevel_sequence)  # Maximum value in the multilevel sequence
    
    for i in range(len(multilevel_sequence)):
        # Generate data values for each symbol
        for j in range(length_symbol):
            value = random.gauss(multilevel_sequence[i], max_seq * noise_prop / 4) / max_seq
            dataset.append(value)
        
        if i != len(multilevel_sequence) - 1:
            # Generate data values for the transition period
            for j in range(length_transition):
                transition_factor = (multilevel_sequence[i] - multilevel_sequence[i + 1]) * j / length_transition
                value = random.gauss(multilevel_sequence[i] - transition_factor, max_seq * noise_prop) / max_seq
                dataset.append(value)
        else:
            # Generate data values for the transition period at the end
            for j in range(length_transition):
                transition_factor = (multilevel_sequence[i] - multilevel_sequence[i - 1]) * j / length_transition
                value = random.gauss(multilevel_sequence[i] - transition_factor, max_seq * noise_prop) / max_seq
                dataset.append(value)

    return dataset  # Return the generated data input as a list
 

def create_dataset(length_sequence, number_of_level, num_channels, length_symbol, length_transition, noise_prop):
    all_datasets = []  # List to store the generated datasets
    all_sequences = create_sequence(length_sequence, number_of_level, num_channels)  # Create different sequences
    
    # Generate data for each sequence and append to the list
    for f in range(len(all_sequences)):
        dataset = generate_data_input(all_sequences[f], length_symbol, length_transition, noise_prop)
        all_datasets.append(dataset)
    
    # Convert the list of datasets to a 3D array with the specified data type
    all_datasets = np.array(all_datasets).astype(np.float32)
    return all_datasets  # Return the generated dataset

def decision(prediction, max_ampl, delay, trigger):
    """
    Makes a channel decision based on predictions and specified criteria.

    Args:
        prediction (numpy.ndarray): A 2D array containing predicted values for different channels.
        max_ampl (float): The maximum amplitude value for trigger comparison.
        delay (int): The delay period to consider consecutive values above the trigger.
        trigger (float): The trigger threshold, as a factor of max_ampl.

    Returns:
        list: A list containing the channel decisions for each step.

    Note:
        This function iterates through predicted values for each step and makes channel decisions
        based on the trigger threshold and consecutive above-threshold conditions. The algorithm tracks
        the history of channel decisions and monitors for changes that satisfy a certain condition.
        If the specified criteria are met, the function updates the channel decision.
    """
    output = []  # List to store the channel decisions
    last_channel = 0  # Initialize the last channel as 0
    new_channel = 0  # Initialize the new channel as 0
    number_above = 0  # Initialize the count of consecutive values above the trigger threshold
    
    for i in range(len(prediction[0])):
        if prediction[last_channel][i] > trigger * max_ampl:
            # Collect predicted values across all channels for the current step
            value_channels = [prediction[j][i] for j in range(len(prediction))]
            # Find the channel with the minimum predicted value
            max_channel = min(range(len(value_channels)), key=lambda j: value_channels[j])
            
            # Check if the current channel change satisfies the condition
            if max_channel != last_channel and (prediction[last_channel][i] - prediction[max_channel][i]) > 0.2:
                if max_channel == new_channel:
                    number_above += 1
                else:
                    new_channel = max_channel
                    number_above = 0
                
                # Update the channel decision if consecutive above-threshold conditions are met
                if number_above > delay:
                    last_channel = max_channel
                    number_above = 0
        else:
            delay = 0  # Reset the delay if the current value is below the trigger
        
        output.append(last_channel)  # Append the current channel decision to the output list
    
    return output  # Return the list of channel decisions

    

def singleDecision(singleData, max_ampl, delay, trigger, lastChannel):
    minIndice = int(np.argmin(singleData))  # Find the index of the minimum value in the data

    for i in range(delay - 1):
        if lastChannel[i] != lastChannel[i + 1]:
            return lastChannel[-1]  # If there is a channel change within the delay period, return the last channel
    
    # Compare the difference between the minimum value and the previous channel value with a threshold
    if (singleData[minIndice] - singleData[int(lastChannel[-1])]) <= -(max_ampl * trigger):
        return minIndice  # If the condition is met, return the index of the minimum value
    else:
        return lastChannel[-1]  # Otherwise, return the last channel
    


def loadhd5(model_path):
    # Load model architecture from JSON
    json_file = open(model_path + 'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = tf.keras.models.model_from_json(loaded_model_json)

    # Compile the loaded model with desired metrics
    loaded_model.compile(loss='mse', optimizer='adam', metrics=['acc', 'mse'])

    # Load weights into the compiled model
    loaded_model.load_weights(model_path + "model.h5")
    
    return loaded_model

def multi_plots(original, predictions, time_array, name_model, model_number, folder_path, score, n_channel, verbose):
    name_model = name_model[:-2] + str(model_number)  # Generate the model name
    for i in range(0, n_channel):
        if i < 10:
            name = '0' + str(i)
        else:
            name = str(i)

        fig = plt.figure()  # Create a new figure
        plt.plot(time_array, original[i], label="original", color="b")  # Plot original data
        plt.plot(time_array, predictions[i], label="prediction", color="r")  # Plot predicted data
        plt.xlabel('time (s)')
        plt.ylabel('occupancy')
        plt.title("Channel " + str(i) + ", RMSE = " + str(score[i]))  # Set title with RMSE value

        fig.tight_layout()

        # Save the plot as an image
        fig.savefig(folder_path + 'original_vs_predicted_' + str(name) + '.png',
                    bbox_inches='tight', dpi=300)
        if verbose:
            plt.show()  # Show the plot if verbose mode is enabled
        plt.clf()  # Clear the figure for the next iteration
        plt.close()  # Close the current figure

def compute_rmse(output, input):
    n_channel = len(input)  # Number of channels
    error = np.subtract(output, input)  # Calculate the element-wise difference between output and input data
    rmse = np.empty(n_channel)  # Create an array to store RMSE values for each channel
    
    for i in range(n_channel):
        rmse[i] = np.sqrt(np.mean(np.square(error[i])))  # Calculate RMSE for the current channel
        
    return np.around(rmse, 4)  # Return RMSE values rounded to four decimal places

def write_data(file_path, data,id):
    with open(file_path+'data'+str(id)+'.txt', 'w') as outfile:
        # I'm writing a header here just for the sake of readability
        # Any line starting with "#" will be ignored by numpy.loadtxt
        outfile.write('# Array shape: {0}\n'.format(data.shape))
        
        # Writing the entire 2D array to the file
        np.savetxt(outfile, data, fmt='%-7.2f')

        # Writing out a break to indicate the end of data
        outfile.write('# End of data\n')

def read_data(file_path,id):
    # Read the array from disk
    new_data = np.loadtxt(file_path+'data'+str(id)+'.txt')
    #new_data=np.reshape(new_data,(n_channel))
    print("shape loaded data : ",np.shape(new_data))
    return(new_data.astype(np.float32))
        

def live_generation(actual_step, model_info, generation_parameters):
    noise = generation_parameters['noise']  # Noise level
    length_symbol = generation_parameters['length_symbol']  # Length of each symbol
    length_transition = generation_parameters['length_transition']  # Length of transition period
    sequence = generation_parameters['sequence']  # Sequence of symbols

    n_channel = model_info['n_channel']  # Number of channels
    output_data = np.empty((n_channel, model_info['n_output']))  # Create an array to store generated output data

    already_generated = actual_step  # Current step
    actual_sequence = already_generated // (length_symbol + length_transition)  # Calculate current sequence index
    pos_in_sequence = already_generated % (length_symbol + length_transition)  # Calculate position within sequence
    max_seq = np.amax(sequence)  # Maximum value in the sequence

    for i in range(n_channel):
        seq = sequence[i][actual_sequence]  # Current symbol in the sequence
        next_seq = sequence[i][actual_sequence + 1]  # Next symbol in the sequence

        for j in range(model_info['n_output']):
            if pos_in_sequence + j <= length_symbol:  # Within the symbol period
                output_data[i, j] = random.gauss(seq, max_seq * noise / 4) / max_seq
            elif pos_in_sequence + j <= length_symbol + length_transition:  # Within the transition period
                transition_factor = (seq - next_seq) * (pos_in_sequence + j - length_symbol) / length_transition
                output_data[i, j] = random.gauss(seq - transition_factor, max_seq * noise) / max_seq
            else:  # After the transition period
                output_data[i, j] = random.gauss(next_seq, max_seq * noise / 4) / max_seq

    return output_data.astype(np.float32)  # Return the generated output data

def full_data_forecast(all_datasets, model_info, interpreter, mapping):
    output = np.empty_like(all_datasets)  # Create an empty array to store the forecasted data

    # Loop through the input datasets with a step size of (n_input + n_output)
    for i in range(0, np.shape(all_datasets)[1] - (model_info['n_input'] + model_info['n_output']), model_info['n_input'] + model_info['n_output']):
        output[:, i:i + model_info['n_input']] = all_datasets[:, i:i + model_info['n_input']]  # Copy the input sequence

        # Make predictions for the next 'n_output' steps using the provided interpreter and mapping
        output[:, i + model_info['n_input']:(i + model_info['n_input'] + model_info['n_output'])] = make_prediction(
            all_datasets[:, i:i + model_info['n_input']], model_info['n_input'], interpreter, model_info['n_output'], mapping)

    # Copy the last 'n_output' steps from the original dataset to the output
    output[:, -model_info['n_output']:] = all_datasets[:, -model_info['n_output']:]

    return output  # Return the forecasted data

def split_in_channel(input, nchannel):
    N_per_split = len(input) // nchannel  # Calculate the number of elements per channel
    output = np.empty((nchannel, N_per_split))  # Create an empty array to store the split data

    for i in range(nchannel):
        start_idx = i * N_per_split  # Calculate the starting index for the current channel
        end_idx = (i + 1) * N_per_split  # Calculate the ending index for the current channel
        output[i] = input[start_idx:end_idx]  # Extract the data for the current channel and store it

    return output  # Return the split data, organized into channels

def compute_envelope(signal, num_points):
    x = np.arange(len(signal))  # Create an array of indices representing the original signal
    cs = CubicSpline(x, signal)  # Create a cubic spline interpolation of the signal
    new_x = np.linspace(0, len(signal) - 1, num_points)  # Generate new x values for interpolation
    envelope = cs(new_x)  # Interpolate the signal using the cubic spline
    return envelope  # Return the interpolated envelope

def gather_data(sdr, n_output, center_freq, num_channel, sample_rate, num_samples):
    ########################### INITIALISATION ###################################
    output_per_run = int(n_output)  # Number of outputs to gather per run
    ##############################################################################

    channel_occupancy = np.empty((num_channel, output_per_run))  # Array to store channel occupancy data
    fftfreqs = np.fft.fftfreq(num_samples, 1 / sample_rate) + center_freq  # Calculate FFT frequency bins
    NFREQ = len(fftfreqs)  # Total number of frequency bins
    idx = np.argsort(fftfreqs)  # Sort indices of frequency bins
    fftfreqs = fftfreqs[idx]  # Sort frequency bins

    for i in range(output_per_run):
        # Receive samples from PlutoSDR
        data = sdr.rx()  # 'data' will contain the received samples in a complex array

        module = np.abs(data)  # Calculate the magnitude of the received samples
        ps = np.fft.fft(module)  # Compute FFT of the magnitude
        ps = np.abs(ps[idx])  # Sort and take the absolute values of FFT coefficients
        flog = np.log10(ps)  # Take the base-10 logarithm of the FFT coefficients
        flog = flog[idx]  # Sort the logarithm values

        BP = np.empty((num_channel))  # Array to store Band Power values for each channel
        for j in range(num_channel):
            # Calculate Band Power for the current channel by summing and averaging
            BP[j] = np.multiply(np.sum(flog[j * NFREQ // num_channel:(j + 1) * NFREQ // num_channel]),
                                NFREQ // num_channel)

        channel_occupancy[:, i] = BP  # Store calculated Band Power values for the current run

    return channel_occupancy  # Return the channel occupancy data


def filter_data(previous_data, channel_occupancy, total_time, max_value, min_value):
    N = 4  # Number of samples per channel occupancy data
    NUM_CHANNEL = len(channel_occupancy)  # Total number of channels
    data_size = len(channel_occupancy[0])  # Size of each channel occupancy data
    full_data = np.empty_like(previous_data)  # Create an empty array with the shape of 'previous_data'

    # Concatenate channel occupancy data with previous data for each channel
    for i in range(NUM_CHANNEL):
        full_data[i] = np.concatenate((previous_data[i, int(data_size):], channel_occupancy[i]))

    full_data_size = len(full_data[0])  # Total size of the combined data for all channels
    channel_envelopes = np.empty((NUM_CHANNEL, full_data_size * N))  # Array to store channel envelopes

    T = total_time / (data_size * N)  # Sample period
    fs = 1 / T  # Sample rate in Hz
    cutoff = 2  # Desired cutoff frequency of the filter in Hz (slightly higher than actual 1.2 Hz)
    nyq = 0.5 * fs  # Nyquist Frequency
    order = 2  # Order of the filter (sin wave approximated as quadratic)
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low')  # Design Butterworth low-pass filter

    # Calculate channel envelopes, filter them, and normalize to the specified range
    for x in range(NUM_CHANNEL):
        channel_envelopes[x] = compute_envelope(full_data[x], full_data_size * N)
        channel_envelopes[x] = filtfilt(b, a, channel_envelopes[x], padlen=order * 3)
        channel_envelopes[x] = 100 * (channel_envelopes[x] - min_value) / (max_value - min_value)

    output = channel_envelopes[:, -data_size:]  # Extract the filtered and normalized channel envelopes

    # Return the original 'previous_data' and the filtered 'output' channel envelopes
    return previous_data, output

def plotting(test,time_array,decideur,name_model):
    colors = ['red', 'green', 'blue', 'cyan', 'magenta', 'yellow', 'black', 'white', 'orange', 'purple']
    # Create a figure and subplots
    if (len(test)>4):
        nbr_plot=4
    else:
        nbr_plot=len(test)

    fig, axs = plt.subplots(nbr_plot+1, 1, figsize=(12, 6))

    for i in range (nbr_plot):
        axs[i].plot(time_array,test[i],color = colors[i])
        axs[i].set_title('Channel ' + str(i))

    # Plot the fifth graph
    axs[nbr_plot].plot(time_array,decideur)
    axs[nbr_plot].set_title('Decideur')

    # Adjust spacing between subplots
    plt.tight_layout()

    # Saving the plot as an image
    fig.savefig('decision_bolck' + str(name_model) + '.png',
                bbox_inches='tight', dpi=300)