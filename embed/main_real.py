import numpy as np
import time
import imse_library as imse
import argparse
import adi

def main(path_model, model_name, load_method, 
         forecast_method,load_data,verbose):

    #################### LOAD AND CONVERT EXISTING MODEL #########################
    if verbose: 
        print('----> Start of conversion to TFLite <----')
    if load_method==0:
        interpreter, mapping, model_info = imse.convert_to_tflite(imse.loadhd5(path_model), path_model + "tfLiteModel")
    elif load_method==1:
        interpreter, mapping, model_info = imse.load_tflite_model(path_model + model_name)
    else:
        print("Error in model loading")
    if verbose: 
        print('----> End of conversion <----')
    
    if verbose: 
        print("model info: ", model_info)


    if forecast_method==0:
    # +++++++++++++++++++++++++++++++++++++++++++++++ DATASET GENERATION +++++++++++++++++++++++++++++++++++++++++++++ #
       
        all_datasets=imse.read_data(path_model)
        if verbose:
            print("----> Data loaded <----")
    
        ################# MAKE PREDICTION FOR EACH ROUND ###############
        t_start = time.time()
        output=imse.full_data_forecast(all_datasets,model_info,interpreter,mapping)
        total_time = time.time() - t_start
        avg_time = total_time * model_info['n_output'] / np.shape(all_datasets)[1]
        if verbose: 
            print("total time (in s): ", total_time)
            print("avg forecast time (in ms): ", avg_time * 1000)

        ######################## COMPUTE RMSE ##########################
        rmse = imse.compute_rmse(output, all_datasets)

        ################# PLOT PREDICTION VS REAL ######################
        imse.multi_plots(all_datasets, output, "testName", 0, path_model, rmse, model_info['n_channel'],verbose)


    if forecast_method==1:
        # SDR parameters
        center_freq=2.448e9
        sample_rate = 60e6  # Hz CORRESPOND TO OUTPUT DATA RATE
        PRECISION=5
        num_samples = 1024*PRECISION  # Number of samples to read in each iteration
        # Connect to the PlutoSDR device
        gain = 20  # dB
        sdr = adi.Pluto('ip:192.168.50.85')
        sdr.sample_rate = int(sample_rate)
        sdr.rx_buffer_size = num_samples  # this is the buffer the Pluto uses to buffer samples
        sdr.gain_control_mode_chan0 = 'manual'
        sdr.rx_hardwaregain_chan0 = gain  # dB. Allowable range is 0 to 74.5 dB
        bandwidth = 70e6  # Bandwidth in Hz (20 MHz for Wi-Fi channels)
        sdr.rx_rf_bandwidth = int(bandwidth)
        sdr.rx_lo = int(center_freq)
        min_value=250000
        max_value=400000
        

        Nrun=1000
        t_start = time.time()
        real=np.empty((model_info['n_channel'],Nrun*(model_info['n_output']+model_info['n_input'])))
        forecast=np.empty_like(real)
        previous_data=imse.gather_data(sdr,100*model_info['n_input'],center_freq,model_info['n_channel'],sample_rate,num_samples)
        for i in range(Nrun):
            data=imse.gather_data(sdr,model_info['n_input']+model_info['n_output'],center_freq,model_info['n_channel'],sample_rate,num_samples)
            previous_data,input=imse.filter_data(previous_data,data,1000*0.001,max_value,min_value)
            real[:,i*(model_info['n_input']+model_info['n_output']):(i+1)*(model_info['n_input']+model_info['n_output'])]=input
            input=input[:,:model_info['n_input']]
            output=imse.make_prediction(input,model_info['n_input'],interpreter,model_info['n_output'],mapping)
            forecast[:,i*(model_info['n_input']+model_info['n_output']):(i)*(model_info['n_input']+model_info['n_output'])+model_info['n_input']]=input
            forecast[:,(i)*(model_info['n_input']+model_info['n_output'])+model_info['n_input']:(i+1)*(model_info['n_input']+model_info['n_output'])]=output

       

        total_time = time.time() - t_start
        avg_time = total_time * model_info['n_output'] / np.shape(real)[1]
        if verbose: 
            print("total time (in s): ", total_time)
            print("avg forecast time (in ms): ", avg_time * 1000)
        real=np.multiply(real,0.01)
        forecast=np.multiply(forecast,0.01)
        time_array=np.linspace(0,total_time,(model_info['n_input']+model_info['n_output'])*Nrun)
        ######################## COMPUTE RMSE ##########################
        rmse = imse.compute_rmse(forecast, real)
        amplitude_max=1
        delay=3
        trigger=0.05
        decideur=imse.decision(forecast,amplitude_max,delay,trigger)
        imse.plotting(forecast,time_array,decideur,"testModel")
        ################# PLOT PREDICTION VS REAL ######################
        imse.multi_plots(real, forecast, time_array, "testName", 0, path_model, rmse, model_info['n_channel'],verbose)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Program inputs.')
    parser.add_argument('-P', '--path-model', type=str,
                        help='Path to the model, default=""', default="", required=False)
    parser.add_argument('-MN', '--model-name', type=str,
                        help='Name of the TFLite model, default="tfLiteModel"', default="tfLiteModel", required=False)
    parser.add_argument('-LM', '--load-method', type=int,
                        help='Which type of model to load (0 : load keras model, 1 : load TFLite model), default=0', default=0, required=False)
    parser.add_argument('-FM', '--forecast-method', type=int,
                        help='Which type of forecasting to do (0 : full data prediction, 1 : live prediction), default=1', default=1, required=False)
    parser.add_argument('-LD', '--load-data', type=int,
                        help='Chose if we are creating or loading data when using FM 0 (0:create data, 1 load data), default=0', default=0, required=False)
    parser.add_argument('-V', '--verbose',
                        help='debeug mode', action='store_true',)

    args = parser.parse_args()
    main(
        args.path_model, args.model_name, args.load_method , args.forecast_method, args.load_data, args.verbose
    )





