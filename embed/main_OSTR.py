import numpy as np
import time
import imse_library as imse
import argparse
import adi
import threading
import matplotlib.pyplot as plt
import queue

# Create queue instances for thread communication
input_queue = queue.Queue()  # Queue to hold input data for CNN
output_queue = queue.Queue()  # Queue to hold output data from CNN
result_queue = queue.Queue()  # Queue to store final channel selection results

id = 3  # Identifier for data logging

def PlutoSDR(exitEvent, inputTaken, sdr, model_info, center_freq, max_value, min_value, sample_rate, num_samples):
    """
    Collect data from the PlutoSDR device, apply filtering, and enqueue for CNN.

    Args:
        exitEvent (threading.Event): Event to signal thread termination.
        inputTaken (threading.Event): Event to indicate input data availability.
        sdr: PlutoSDR instance for data acquisition.
        model_info (dict): Information about the model.
        center_freq (float): Center frequency for data acquisition.
        max_value (int): Maximum valid data value.
        min_value (int): Minimum valid data value.
        sample_rate (float): Sample rate for data acquisition.
        num_samples (int): Number of samples to read in each iteration.

    Returns:
        None
    """
    previous_data = imse.gather_data(sdr, 100 * model_info['n_input'], center_freq, model_info['n_channel'], sample_rate, num_samples)
    logData = []
    nRun = 0

    while not exitEvent.is_set():
        data = imse.gather_data(sdr, model_info['n_input'], center_freq, model_info['n_channel'], sample_rate, num_samples)
        previous_data, input_data = imse.filter_data(previous_data, data, 1000 * 0.001, max_value, min_value)
        logData.append(input_data)
        nRun += 1

        input_queue.put(input_data)  # Enqueue input data for CNN
        inputTaken.wait(timeout=1)
        inputTaken.clear()

    logData = np.transpose(np.array(logData), axes=(1, 0, 2))
    logData = logData.reshape(model_info['n_channel'], model_info['n_input'] * nRun)
    imse.write_data("LogData/", logData, id)  # Log processed data
    return()

def CNN(exitEvent, inputTaken, outputTaken, interpreter, model_info, mapping):
    """
    Perform CNN prediction on input data and enqueue the output.

    Args:
        exitEvent (threading.Event): Event to signal thread termination.
        inputTaken (threading.Event): Event to indicate input data availability.
        outputTaken (threading.Event): Event to indicate output data availability.
        interpreter: TensorFlow Lite interpreter for prediction.
        model_info (dict): Information about the model.
        mapping: Mapping information for input and output tensors.

    Returns:
        None
    """
    while not exitEvent.is_set():
        data = input_queue.get()  # Dequeue input data from PlutoSDR thread
        inputTaken.set()
        output = imse.make_prediction(data, model_info['n_input'], interpreter, model_info['n_output'], mapping)
        output_queue.put(output)  # Enqueue output data for decision-making
        outputTaken.wait(timeout=1)
        outputTaken.clear()

    return()

def Decide(exitEvent, outputTaken, Nrun, decisionParam):
    """
    Make channel selection decisions based on CNN output and store results.

    Args:
        exitEvent (threading.Event): Event to signal thread termination.
        outputTaken (threading.Event): Event to indicate output data availability.
        Nrun (int): Number of runs in the loop.
        decisionParam (dict): Parameters for decision-making.

    Returns:
        None
    """
    lastChannel = [0 for i in range(decisionParam["delay"])]
    currentIndice = 0
    channelSelection = []

    for i in range(Nrun):
        if (i / Nrun) * 100 % 1 == 0:
            print(int(i / Nrun * 100), "%")
        data = output_queue.get()  # Dequeue CNN output data
        outputTaken.set()

        for j in range(len(data[0])):
            selectedChannel = imse.singleDecision(data[:, j], decisionParam["amplitude"], decisionParam["delay"], decisionParam["trigger"], lastChannel[currentIndice:] + lastChannel[:currentIndice])
            lastChannel[currentIndice] = selectedChannel
            currentIndice = (currentIndice + 1) % decisionParam["delay"]
            channelSelection.append(selectedChannel)

    exitEvent.set()
    result_queue.put(channelSelection)  # Store channel selection results
    return()


def main(path_model, model_name, load_method, 
         forecast_method,Nrun,verbose):
    """
    Main function to load models, perform predictions, and display results.

    Args:
        path_model (str): Path to the model.
        model_name (str): Name of the TFLite model.
        load_method (int): Method for loading the model (0: Keras, 1: TFLite).
        forecast_method (int): Type of forecasting (0: full data, 1: live).
        Nrun (int): Number of runs in the loop.
        verbose (bool): Debug mode flag.

    Returns:
        None
    """

    #################### LOAD AND CONVERT EXISTING MODEL #########################
    if verbose: 
        print('----> Start of conversion to TFLite <----')
    if load_method==0:
        interpreter, mapping, model_info = imse.convert_to_tflite(imse.loadhd5(path_model), path_model + "tfLiteModel")
    elif load_method==1:
        interpreter, mapping, model_info = imse.load_tflite_model(path_model + model_name)
    else:
        print("Error in model loading")
    if verbose: 
        print('----> End of conversion <----')
    
    if verbose: 
        print("model info: ", model_info)


    if forecast_method==0:
    # +++++++++++++++++++++++++++++++++++++++++++++++ DATASET GENERATION +++++++++++++++++++++++++++++++++++++++++++++ #
       
        all_datasets=imse.read_data('LogData/',id)
        if verbose:
            print("----> Data loaded <----")
    
        ################# MAKE PREDICTION FOR EACH ROUND ###############
        t_start = time.time()
        output=imse.full_data_forecast(all_datasets,model_info,interpreter,mapping)
        total_time = time.time() - t_start
        avg_time = total_time * model_info['n_output'] / np.shape(all_datasets)[1]
        if verbose: 
            print("total time (in s): ", total_time)
            print("avg forecast time (in ms): ", avg_time * 1000)

        ######################## COMPUTE RMSE ##########################
        rmse = imse.compute_rmse(output, all_datasets)
        timeArray=np.linspace(0,11,len(all_datasets[0]))
        ################# PLOT PREDICTION VS REAL ######################
        imse.multi_plots(all_datasets, output, timeArray, "testName", 0, path_model, rmse, model_info['n_channel'],verbose)


    if forecast_method==1:
        # SDR parameters
        center_freq=2.448e9
        sample_rate = 60e6  # Hz CORRESPOND TO OUTPUT DATA RATE
        PRECISION=5
        num_samples = 1024*PRECISION  # Number of samples to read in each iteration
        # Connect to the PlutoSDR device
        gain = 20  # dB
        sdr = adi.Pluto()
        sdr.sample_rate = int(sample_rate)
        sdr.rx_buffer_size = num_samples  # this is the buffer the Pluto uses to buffer samples
        sdr.gain_control_mode_chan0 = 'manual'
        sdr.rx_hardwaregain_chan0 = gain  # dB. Allowable range is 0 to 74.5 dB
        bandwidth = 70e6  # Bandwidth in Hz (20 MHz for Wi-Fi channels)
        sdr.rx_rf_bandwidth = int(bandwidth)
        sdr.rx_lo = int(center_freq)
        max_value=420000
        min_value=250000
        decisionParam={"amplitude": 100, "delay": 100, "trigger": 0.05}
        t_start = time.time()


        exitEvent=threading.Event()
        inputTaken=threading.Event()
        outputTaken=threading.Event()


        thread1 = threading.Thread(target=PlutoSDR, args=(exitEvent, inputTaken, sdr, model_info, center_freq, max_value, min_value, sample_rate, num_samples,))
        thread2 = threading.Thread(target=CNN, args=(exitEvent, inputTaken, outputTaken, interpreter, model_info, mapping,))
        thread3 = threading.Thread(target=Decide, args=(exitEvent, outputTaken, Nrun, decisionParam,))

        thread1.start()
        thread2.start()
        thread3.start()

        thread1.join()
        thread2.join()
        thread3.join()

        total_time = time.time() - t_start
       

        
        avg_time = total_time / (Nrun*model_info['n_output'])
        if verbose: 
            print("total time (in s): ", total_time)
            print("avg forecast time (in ms): ", avg_time * 1000)
        
        resultChannel=result_queue.get()
        time_array=np.linspace(0,total_time,len(resultChannel))
        plt.figure()
        plt.title("Channel selected through time")
        plt.ylabel("Channel")
        plt.xlabel("time (s)")
        plt.xlim(time_array[0], time_array[-1])
        plt.ylim(min(resultChannel)-2, max(resultChannel)+2)
        length=len(resultChannel)
        for i in range(0,length,length//10):
            plt.plot(time_array[:i],resultChannel[:i],'b-')
            plt.pause(total_time/(10*len(resultChannel)))
        plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Program inputs.')
    parser.add_argument('-P', '--path-model', type=str,
                        help='Path to the model, default=""', default="", required=False)
    parser.add_argument('-MN', '--model-name', type=str,
                        help='Name of the TFLite model, default="tfLiteModel"', default="tfLiteModel", required=False)
    parser.add_argument('-LM', '--load-method', type=int,
                        help='Which type of model to load (0 : load keras model, 1 : load TFLite model), default=0', default=0, required=False)
    parser.add_argument('-FM', '--forecast-method', type=int,
                        help='Which type of forecasting to do (0 : full data prediction, 1 : live prediction), default=1', default=1, required=False)
    parser.add_argument('-LD', '--load-data', type=int,
                        help='Chose if we are creating or loading data when using FM 0 (0:create data, 1 load data), default=0', default=0, required=False)
    parser.add_argument('-NR', '--nrun', type=int,
                        help='How many run to do in the loop (default=100)', default=100, required=False)
    parser.add_argument('-V', '--verbose',
                        help='debeug mode', action='store_true',)

    args = parser.parse_args()
    main(
        args.path_model, args.model_name, args.load_method , args.forecast_method, args.nrun, args.verbose
    )





