
import numpy as np
import adi
import matplotlib.pyplot as plt
import time
import scipy
from scipy import signal
from scipy.signal import hilbert
import cv2
from multiIO import split_dataset, evaluate_model_cnn, multi_plots
from decision_block import DecisionBlock

''' ************************************************ CUSTOM METHODS ********************************************** '''


def bandpower(x, fs, fmin, fmax):
    f, Pxx = scipy.signal.periodogram(x, fs=fs)
    ind_min = np.argmax(f > fmin) - 1
    ind_max = np.argmax(f > fmax) - 1
    return np.trapz(Pxx[ind_min: ind_max], f[ind_min: ind_max])


''' ********************************************* INITIALIZE VARIABLES ******************************************** '''
# Folder path to save data
folder_path = 'study/20_channels/'

# Number of channels
num_channels = 20
chan_inc = 0.5/num_channels  # Increments for bandpower calculation.

# ---------------------------------------------- SDR parameters ------------------------------------------------------ #
sample_rate = 1e6  # Hz
center_freq = 2.45e9  # Hz
num_samples = 30000   # number of samples returned per call to rx()
gain = 70   # dB
num_runs = 5000

# ---------------------------------------------- Network parameters -------------------------------------------------- #
# define the percentage of the dataset that will be used for training (remaining will be used for prediction/test)
train_percentage = 75

# define the total samples to use as input and number of output samples to be predicted.
n_input = 5
n_output = 50

# load saved network model
load_model = False

# -------------------------------------------- Decision block parameters --------------------------------------------- #
all_thresholds = [0.4, 0.8]                 # list containing all threshold values
num_inputs = 1                              # number of inputs = time steps
input_tolerance = 0.05

''' *********************************************** CAPTURE SIGNALS ********************************************** '''
sdr = adi.Pluto()
sdr.sample_rate = int(sample_rate)
sdr.rx_rf_bandwidth = int(sample_rate)  # filter cutoff, just set it to the same as sample rate

sdr.rx_lo = int(center_freq)    # rx_lo: Carrier frequency of RX path
sdr.rx_buffer_size = num_samples  # this is the buffer the Pluto uses to buffer samples
sdr.gain_control_mode_chan0 = 'manual'
sdr.rx_hardwaregain_chan0 = gain   # dB. Allowable range is 0 to 74.5 dB

all_data = num_runs * [0]

print('Capturing signals')
start_time = time.time()
for i in range(0, num_runs):
    samples = sdr.rx()  # receive samples off Pluto
    all_data[i] = samples
end_time = time.time()
print('****** Signals captured ****** --> seconds elapsed:', end_time - start_time)

''' ************************************* GET FFT AND PSD OF CAPTURED SIGNAL ************************************* '''
''' We plot the FFT of a single instance, since plotting all 5000 time instances takes time '''

data_i = all_data[0]     # choose time instance to calculate FFT.
Fs = sample_rate  # Hz. Let's say we sampled at 1 MHz.
N = num_samples   # number of points to simulate, and our FFT size.
f = np.arange(Fs/-2.0, Fs/2.0, Fs/N)    # start, stop, step.  centered around 0 Hz

# FFT function
s = np.fft.fft(data_i)
plt.figure(0)
plt.plot(f, s, '.-')
plt.savefig(folder_path + 'FFT' + '.png', bbox_inches='tight', dpi=300)
plt.title('FFT')
plt.ylabel('FFT of signal')
plt.xlabel('FFT index')

# Plot the magnitude and phase.
S_mag = np.abs(s)
S_phase = np.angle(s)

plt.figure(1)
plt.plot(f, S_mag, '.-')
plt.title('FFT - Magnitude')
plt.ylabel('FFT of signal (Magnitude)')
plt.xlabel('FFT index')
plt.savefig(folder_path + 'FFT Magnitude' + '.png', bbox_inches='tight', dpi=300)

plt.figure(2)
plt.plot(f, S_phase, '.-')
plt.title('FFT - Phase')
plt.ylabel('FFT of signal (Phase)')
plt.xlabel('FFT index')
plt.savefig(folder_path + 'FFT Phase' + '.png', bbox_inches='tight', dpi=300)
plt.show()

# FFT shift (Place 0 Hz (DC) in the center and negative frequencies to the left)
S = np.fft.fftshift(s)
plt.figure(3)
plt.plot(f, S, '.-')
plt.title('FFT - Shifted')
plt.ylabel('FFT of signal')
plt.xlabel('FFT index')
plt.savefig(folder_path + 'FFT Shift' + '.png', bbox_inches='tight', dpi=300)
plt.show()

# Power Spectral Density (PSD)
# data_i = data_i * np.hamming(len(data_i))  # (OPTIONAL) apply a Hamming window
PSD = (np.abs(np.fft.fft(data_i))/N)**2
PSD_log = 10.0*np.log10(PSD)    # Convert to dB to view PSDs in log scale.
PSD_shifted = np.fft.fftshift(PSD_log)  # Move to “0 Hz”

f += center_freq    # now add center frequency
plt.figure(4)
plt.plot(f, PSD_shifted)
plt.title('PSD')
plt.xlabel("Frequency [Hz]")
plt.ylabel("Magnitude [dB]")
plt.grid(True)
plt.savefig(folder_path + 'PSD Shift' + '.png', bbox_inches='tight', dpi=300)
plt.show()

''' ******************************************** COMPUTING BANDPOWER ********************************************* '''
ch = 0

rows, cols = (num_channels, num_runs)
oc = [[0 for i in range(cols)] for j in range(rows)]
Oc = num_runs * [0]

print('Computing bandpower')
start_time2 = time.time()
for m in range(0, num_channels):
    chl = ch
    chu = ch + chan_inc
    for n in range(0, num_runs):
        oc_i = bandpower(all_data[n], 1, chl, chu)
        oc[m][n] = oc_i
    ch = ch + chan_inc
end_time2 = time.time()
print('****** Bandpower computed ****** --> seconds elapsed:', end_time2 - start_time2)

oc = np.array(oc)

# Normalize bandpower between 0 - 1
out = np.zeros(oc.shape, np.double)
oc2 = cv2.normalize(oc, out, 0, 1, cv2.NORM_MINMAX)

''' **************************************** ENVELOPE OF 'N' CHANNELS ******************************************** '''
channel_envelopes = num_channels*[0]

print('Computing envelope')
start_time3 = time.time()
for x in range(0, num_channels):
    cha_i = oc[x]

    analytic_signal = hilbert(cha_i, N=5000)
    amplitude_envelope = np.abs(analytic_signal)

    # Normalize data between 0 - 1
    amplitude_envelope = amplitude_envelope / np.max(amplitude_envelope)

    channel_envelopes[x] = amplitude_envelope

end_time3 = time.time()
print('****** Envelope computed ****** --> seconds elapsed:', end_time3 - start_time3)

# Normalize envelopes
channel_envelopes = np.array(channel_envelopes)
out = np.zeros(oc.shape, np.double)
channel_envelopes_all = cv2.normalize(channel_envelopes, out, 0, 1, cv2.NORM_MINMAX)

for x in range(0, num_channels):
    cha_i = oc2[x]

    amp_envelope = channel_envelopes_all[x]

    fig = plt.figure()
    ax0 = fig.add_subplot(111)
    ax0.plot(cha_i)
    ax0.plot(amp_envelope)
    plt.title('Envelope ' + 'channel ' + str(x+1))
    plt.savefig(folder_path + 'Envelope' + 'channel' + str(x+1) + '.png', bbox_inches='tight', dpi=300)
# plt.show()

np.save(folder_path + 'channels_env_' + str(num_channels), channel_envelopes)

''' ************************************** NETWORK TRAINING AND PREDICTIONS ************************************** '''
# channel_envelopes = np.load('channels_env.npy')

# identification names of network models
name_model = 'cnn'

dataset = np.array(channel_envelopes_all)

# split into train and test
train, test = split_dataset(dataset, num_channels, train_percentage)

score, scores, original, predictions, forecast_runtime, train_runtime, load_runtime, avg_forecast_times = \
    evaluate_model_cnn(train, test, n_input, n_output, folder_path, name_model, load_model)

# plot original and predicted dataset and save graphical comparison to image file.
multi_plots(original, predictions, name_model, folder_path, score, num_channels)

# save predictions of each channel
channel_predictions = predictions

# runtime
if not load_model:
    t4 = 'Training runtime: ' + str(train_runtime) + ' seconds'
else:
    t4 = 'Load network runtime: ' + str(load_runtime) + ' seconds'


''' ********************************************** DECISION BLOCK ************************************************ '''
channel_inputs = num_channels * [0]

for i in range(num_channels):
    channel_inputs[i] = predictions[i]

start_time4 = time.time()
# split the inputs to "time step" sizes
db = DecisionBlock(channel_inputs, num_inputs, all_thresholds, input_tolerance, folder_path)
channel_inputs = db.split_input(channel_inputs, num_inputs)
data, selected_list = db.decide()
end_time4 = time.time()
print('Deciding occupancy --> seconds elapsed:', end_time4 - start_time4)
# plot band selection
db.loop_plot(data, selected_list)

# save elapsed time data and other data in txt file format.
m1 = 'Number of channels: ' + str(num_channels)
m2 = 'Size of dataset: ' + str(num_runs)
# m3 = 'RMSE: ' + str('%.3f' % score)
m3 = 'RMSE: ' + " ".join(score)
t1 = str(end_time - start_time)
t2 = 'Bandpower computing runtime: ' + str(end_time2 - start_time2)
t3 = 'Envelope computing runtime: ' + str(end_time3 - start_time3)
t5 = 'Average forecast time of each instance: ' + str("%f" % avg_forecast_times) + ' seconds'
t6 = 'Forecast runtime: ' + str("%f" % forecast_runtime) + ' seconds'
t7 = 'Deciding occupancy: ' + str("%f" % (end_time4 - start_time4)) + ' seconds'
time_list = [m1, m2, m3, t1, t2, t3, t4, t5, t6, t7]
np.savetxt(folder_path + 'elapsed_times_' + str(name_model) + '_' + str(num_channels) + '.txt', time_list, delimiter=','
           , fmt='%s')
