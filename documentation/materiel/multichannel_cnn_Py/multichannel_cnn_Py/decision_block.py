import numpy as np
import matplotlib.pyplot as plt
from numpy import array


class DecisionBlock:

    def __init__(self, c_inputs, n_inputs, thresholds, tolerance, folder_path):

        self.folder_path = folder_path
        self.data = array(c_inputs)
        self.n_inputs = n_inputs
        self.thresholds = thresholds

        self.tolerance = tolerance   # tolerance for minor differences between 2 different input values

        self.inputs = self.split_input(c_inputs, self.n_inputs)
        self.len_channels = len(self.inputs[0])  # total number of channels
        self.len_channel_i = len(self.inputs)    # length of a single channel

        self.hold = False
        self.selected_band = 0
        self.previous_band = 0
        self.selected_list = []     # list to store sequence of selected bands

        self.previous_value = 0
        self.current_value = 0

        self.previous_v = 0
        self.next_v = 0
        self.current_v = 0
        self.previous_list = []
        self.current_list = []

        self.previous_th = self.thresholds[0]
        self.current_th = self.thresholds[0]
        self.next_th = self.thresholds[0]
        self.th_selector = 0
        self.th_selected = False

        self.decrease = True
        self.increase = False

        self.values_all = [[] for _ in range(self.len_channels)]    # saves all values that are <= threshold
        self.index_all = []                                         # temp. index value of the signal values
        self.positions = []                                         # real index position of each signal value
        self.channel_ids = []                                       # channel identification of each value
        self.positions_i = []

        self.th_list = []

        self.switch = False

    def split_input(self, data, n_input):
        n_output = n_input
        data = array(data)
        start = 0

        # get original indexes of values
        x = list()
        limit = int(len(data[0])/n_input)

        # step over the entire history one time step at a time
        for _ in range(limit):
            # define the end of the input sequence
            in_end = start + n_input
            out_end = in_end + n_output
            # ensure we have enough data for this instance
            if out_end <= (len(data[0]) + n_input):
                x_input = data[:, start:in_end]
                # x_input = x_input.reshape((len(x_input), 1))
                x.append(x_input)
            # move along one time step
            start += n_input

        return array(x)

    def decide(self):

        for i in range(0, self.len_channel_i):
            cha_i = self.inputs[i]  # read n input values. One for each channel.
            self.current_list = cha_i

            # take the values of each channel and compare with threshold value
            self.compare_threshold(cha_i)

            self.th_selected = False

            self.selected_list.append(self.selected_band + 1)  # save the channel ids of the selected bands. (the +1 is
            # because the index starts from 0, and we don't have Channel 0)

        return self.data, self.selected_list

    def compare_threshold(self, channels):

        # get minimum of the available channels
        self.current_value = min(channels)
        # get channel id of minimum
        id_min = np.argmin(channels)

        # compare minimum with thresholds and chose threshold
        for i in range(0, len(self.thresholds)):
            if self.current_value < self.thresholds[i] and not self.th_selected:
                self.th_selected = True
                self.current_th = self.thresholds[i]

                index = [x for x, z in enumerate(channels) if z <= self.previous_th]
                arr_index = array(index)

                # check if threshold have been changed
                if self.previous_th != self.current_th:
                    self.previous_th = self.current_th
                    self.switch = True

                if self.switch:
                    # get ID/index (channel ID) that is <= current threshold
                    index = [x for x, z in enumerate(channels) if z <= self.current_th]
                    arr_index = array(index)
                    self.switch = False

                if id_min != self.selected_band and self.selected_band not in arr_index:
                    self.selected_band = id_min
        self.previous_value = self.current_value

    def loop_plot(self, data, selected_list):
        figs = plt.figure()
        jet = plt.get_cmap('Set2')
        colors = iter(jet(np.linspace(0, 1, 60)))
        axs = {}
        for idx in range(self.len_channels + 1):

            if idx < self.len_channels:
                axs[idx] = figs.add_subplot(self.len_channels + 1, 1, idx + 1)
                axs[idx].plot(data[idx], color=next(colors))
                axs[idx].set_ylabel('CH' + str(idx + 1))

            if idx == self.len_channels:
                axs[idx] = figs.add_subplot(self.len_channels + 1, 1, idx + 1)
                axs[idx].plot(selected_list, color=next(colors))
                axs[idx].set_ylabel('Band select')
                axs[idx].set_yticks([1, 2, 3, 4, 5])

        figs.tight_layout()

        # plt.plot(self.selected_list)
        plt.savefig(self.folder_path + 'decisions', bbox_inches='tight', dpi=300)
        # plt.show()
        return figs, axs
