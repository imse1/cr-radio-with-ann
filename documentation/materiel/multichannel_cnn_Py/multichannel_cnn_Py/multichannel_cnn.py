
from math import sqrt
import math
import time
import statistics
import keras.metrics
from numpy import split
from numpy import array
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from keras.utils.vis_utils import plot_model
from generate_data_input import generate_data_input
from tensorflow import keras
from keras import layers
from keras.models import model_from_json
import keras.backend as k


# split a univariate dataset into train / test sets
def split_dataset(data, num_signal, train_percent):

    limit_train = int((train_percent / 100) * len(data[0]))
    # create an array,the size of the dataset.
    train_all = np.empty(num_signal, dtype=object)
    test_all = np.empty(num_signal, dtype=object)

    for i in range(0, num_signal):

        data_sing = data[i]
        train_sing = data_sing[0:limit_train]

        test_sing = data_sing[limit_train:len(data[0]) + 1]

        train_all[i], test_all[i] = train_sing, test_sing

    return train_all, test_all


def evaluate_model_cnn(train, test, n_input, n_output, folder_path, name_model, load_model):
    train_runtime = 0
    load_runtime = 0
    n_channels = len(train)
    # fit model
    if not load_model:
        model, train_runtime = build_model_cnn(train, n_input, n_output, folder_path, name_model)
    else:
        load_start = time.time()
        # load model
        model = loadhd5()
        # model = load_model('model.h5')
        load_end = time.time()
        load_runtime = load_end - load_start
        print('Load network runtime --> seconds elapsed:', load_runtime)

    # history is a list of n_input samples
    history_all = []
    test_all = []
    for m in range(0, n_channels):
        # reshape data
        train_sing = train[m].reshape(len(train[m]), 1)
        test_sing = test[m].reshape(len(test[m]), 1)

        # reshape data
        limit_train = math.trunc(int(len(train[m]) / n_output))
        limit_test = math.trunc(int(len(test[m]) / n_output))
        train_sing = train_sing[0:limit_train * n_output]
        test_sing = test_sing[0:limit_test * n_output]

        # restructure into windows of sequence data
        train_sing = array(split(train_sing, int(len(train[m]) / n_output)))
        test_sing = array(split(test_sing, int(len(test[m]) / n_output)))

        # history is a list of n_input samples
        history = [x for x in train_sing]
        history_all.append(history)

        # append test set of individual channels
        test_all.append(test_sing)

    # walk - forward validation over each n_input samples
    predictions = list()
    forecast_times = []
    forecast_start = time.time()
    print('----> Forecasting <----')
    for i in range(len(test_all[0])):
        forecast_i_start = time.time()

        # predict the samples of size n_output
        yhat_sequence = forecast(model, history_all, n_input)

        forecast_i_end = time.time()
        forecast_i_runtime = forecast_i_end - forecast_i_start
        forecast_times.append(forecast_i_runtime)

        # store the predictions
        predictions.append(yhat_sequence)
        # get real observation and add to history for predicting the next samples
        for j in range(0, n_channels):
            history_all[j].append(test_all[j][i, :])

    forecast_end = time.time()
    forecast_runtime = forecast_end - forecast_start
    print('Forecast runtime --> seconds elapsed:', forecast_runtime)

    # calculate average of single forecast times
    forecast_times = array(forecast_times)
    avg_forecast_times = np.average(forecast_times)

    rmse_score = []
    rmse_scores = []
    for m in range(0, n_channels):
        # evaluate predictions of each sample
        predictions = array(predictions)
        score, scores = evaluate_forecasts(test_all[m][:, :, 0], predictions[:, m, 0, :])
        rmse_score.append(score)
        rmse_scores.append(scores)

    predictions_list = list()
    original_list = list()
    for n in range(0, n_channels):
        # reshape the arrays into an easily readable format
        predictions_sing = predictions[:, n, 0, :].reshape((predictions.shape[0] * predictions.shape[3],
                                                            test_all[0].shape[2]))
        predictions_list.append(predictions_sing)
        original = test_all[n][:, :, 0]
        original = original.reshape((original.shape[0] * original.shape[1], test_all[0].shape[2]))
        original_list.append(original)

    return rmse_score, rmse_scores, original_list, predictions_list, forecast_runtime, train_runtime, load_runtime, \
        avg_forecast_times


# train the model
def build_model_cnn(train, n_input, n_output, f_path, model_name):
    n_channels = len(train)

    # define parameters
    verbose, epochs, batch_size = 0, 20, 16

    model_output = np.empty(n_channels, dtype=object)
    model_input = np.empty(n_channels, dtype=object)
    train_x_all = np.empty(n_channels, dtype=object)
    train_y_all = np.empty(n_channels, dtype=object)

    for j in range(0, n_channels):
        # reshape data
        train_sing = train[j].reshape(len(train[j]), 1)
        limit = math.trunc(int(len(train[j]) / n_output))
        train_sing = train_sing[0:limit*n_output]
        # restructure into windows of sequence data
        train_sing = array(split(train_sing, int(len(train[j]) / n_output)))

        # prepare data
        train_x, train_y = to_supervised(train_sing, n_input, n_output)
        train_x_all[j] = train_x
        train_y_all[j] = train_y

    train_x = train_x_all
    train_y = train_y_all

    n_timesteps, n_features, n_outputs = train_x[0].shape[1], train_x[0].shape[2], train_y[0].shape[1]

    for q in range(0, n_channels):
        name_channel = 'channel_' + str(q + 1)
        # define input node
        channel_input = keras.Input(shape=(n_timesteps, n_features), name=name_channel)
        model_input[q] = channel_input

    # Convert array to list
    input_list = model_input.tolist()
    concatenate_input = layers.Concatenate()(input_list)

    # adding a first hidden layer
    layer_1_1 = layers.Conv1D(filters=24, kernel_size=3, activation='relu')(concatenate_input)
    # adding a second hidden layer
    layer_1_2 = layers.MaxPooling1D(pool_size=2)(layer_1_1)
    # adding a third hidden layer
    layer_1_3 = layers.Flatten()(layer_1_2)
    # adding a fourth hidden layer
    layer_1_4 = layers.Dense(10, activation='relu')(layer_1_3)

    for r in range(0, n_channels):
        name_channel = 'out_channel_' + str(r+1)
        # adding the output layer
        output_1 = layers.Dense(n_outputs, activation='linear', name=name_channel)(layer_1_4)
        model_output[r] = output_1

    # **************************************** Instantiate Input and Outputs ***************************************** #
    # Convert arrays to list
    output_list = model_output.tolist()
    train_x_all = train_x_all.tolist()
    train_y_all = train_y_all.tolist()

    model = keras.Model(inputs=input_list,
                        outputs=output_list, name='channels_models')
    # ******************************************** Compiling both inputs ********************************************* #

    # compiling the neural network
    model.compile(loss='mse', optimizer='adam', metrics=['acc', 'mse'])

    # Plot and save network model
    plot_model(model, show_shapes=True, to_file=f_path + model_name + '_network_model.png', dpi=300)

    # fit network
    print('----> Training network <----')
    train_start = time.time()
    model.fit(train_x_all, train_y_all, epochs=epochs, batch_size=batch_size, verbose=verbose)
    train_end = time.time()
    train_runtime = train_end - train_start
    print('Training runtime --> seconds elapsed:', train_runtime)

    savehd5(model, f_path)

    return model, train_runtime


# convert history into inputs and outputs
def to_supervised(train, n_input, n_output):
    # flatten data
    data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
    x, y = list(), list()
    in_start = 0
    # step over the entire history one time step at a time
    for _ in range(len(data)):
        # define the end of the input sequence
        in_end = in_start + n_input
        out_end = in_end + n_output
        # ensure we have enough data for this instance
        if out_end < len(data):
            x_input = data[in_start:in_end, 0]
            x_input = x_input.reshape((len(x_input), 1))
            x.append(x_input)
            y.append(data[in_end:out_end, 0])
        # move along one time step
        in_start += 1
    return array(x), array(y)


# make a forecast
def forecast(model, history, n_input):
    # flatten data
    data = array(history)
    # list of 'n' inputs
    input_all = []
    for w in range(0, len(history)):
        data_sing = data[w].reshape(data[w].shape[0]*data[w].shape[1], 1)
        # retrieve last observations for input data
        input_x = data_sing[-n_input:, 0]
        # reshape into [1, n_input, 1]
        input_x = input_x.reshape((1, len(input_x), 1))
        input_all.append(input_x)
    # forecast the next sample
    yhat = model.predict(input_all, verbose=0)

    return yhat


# evaluate one or more weekly forecasts against expected values
def evaluate_forecasts(actual, predicted):
    scores = list()
    # calculate an RMSE score for each sample
    for i in range(actual.shape[1]):
        # calculate mse
        mse = mean_squared_error(actual[:, i], predicted[:, i])
        # calculate rmse
        rmse = sqrt(mse)
        # store
        scores.append(rmse)
    # calculate overall RMSE
    s = 0
    for row in range(actual.shape[0]):
        for col in range(actual.shape[1]):
            s += (actual[row, col] - predicted[row, col]) ** 2
    score = '%.3f' % (sqrt(s/(actual.shape[0] * actual.shape[1])))
    return score, scores


def multi_plots(original, predictions, name_model, folder_path, score, n_channel):

    alphabet_index = 97
    a_index = 97

    for i in range(0, n_channel):
        if alphabet_index > 122:
            alphabet = chr(alphabet_index)
        else:
            alphabet = chr(alphabet_index) + chr(a_index)

        # Setting the figure
        fig = plt.figure()
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)

        ax1.plot(original[i])
        ax2.plot(predictions[i])

        ax1.set_title('- Original -', fontsize=10)
        ax2.set_title('- Predicted -- ' + score[i], fontsize=10)

        fig.tight_layout()

        # Saving the plot as an image
        fig.savefig(folder_path + 'original_vs_predicted_' + str(name_model) + alphabet + '.png',
                    bbox_inches='tight', dpi=300)

        if alphabet_index > 122:
            if a_index <= 122:
                a_index = a_index + 1
            else:
                alphabet_index = alphabet_index - 1
        else:
            a_index = 97
            alphabet_index = alphabet_index + 1

    # plt.show()


def normalize(data):
    mu = statistics.mean(data)
    sigma = statistics.stdev(data, mu)
    standardized_train = (data - mu) / sigma

    return standardized_train


def savehd5(model, folder_path):
    # serialize model to JSON
    model_json = model.to_json()
    with open(folder_path + "model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(folder_path + "model.h5")
    print("Saved model to disk")


def loadhd5():
    # load json and create model
    json_file = open(folder_path + 'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(folder_path + "model.h5")
    print("----> Loaded model from disk <----")
    return loaded_model


def evalhd5(loaded_model, x, y):
    # evaluate loaded model on test data
    loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    score = loaded_model.evaluate(x, y, verbose=0)
    print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1] * 100))


if __name__ == '__main__':

    # +++++++++++++++++++++++++++++++++++++++++++++ FOLDER PATH TO SAVE DATA +++++++++++++++++++++++++++++++++++++++++ #
    folder_path = 'data/dataset_6/normal/'

    # +++++++++++++++++++++++++++++++++++++++++++++++ DATASET GENERATION +++++++++++++++++++++++++++++++++++++++++++++ #
    # generate dataset
    length_symbol = 100
    length_transition = 70

    multilevel_sequence_1 = [0, 2, 1, 2, 1, 0, 1, 0, 1, 3, 0, 0, 1]
    multilevel_sequence_2 = [0, 2, 1, 2, 1, 0, 1, 0, 1, 2, 3, 0, 1]
    multilevel_sequence_3 = [0, 2, 2, 2, 1, 0, 3, 0, 1, 2, 3, 0, 1]
    multilevel_sequence_4 = [0, 2, 2, 2, 1, 0, 3, 0, 2, 1, 3, 2, 1]

    noise_prop = 0.15

    # number of selected input signals
    num_input_signal = 4

    all_datasets = []

    # NOTE: add any newly defined 'multilevel_sequence' to list of 'all_sequences'
    all_sequences = [multilevel_sequence_1, multilevel_sequence_2, multilevel_sequence_3, multilevel_sequence_4]

    dataset_2 = generate_data_input(multilevel_sequence_2, length_symbol, length_transition, noise_prop)

    for f in range(0, num_input_signal):
        multilevel_sequence = all_sequences[f]
        dataset = generate_data_input(multilevel_sequence, length_symbol, length_transition, noise_prop)
        all_datasets.append(dataset)

    # convert to array list of datasets
    all_datasets = array(all_datasets)

    # define the percentage of the dataset that will be used for training (remaining will be used for prediction/test)
    train_percentage = 75

    # create an array,the size of the dataset.
    total_input = np.empty(num_input_signal, dtype=object)

    # define the total samples to use as input and number of output samples to be predicted.
    num_input = 5
    num_output = 5

    n_input_1 = n_input_2 = num_input
    n_output_1 = n_output_2 = num_output

    name_model = 'm_io'

    # split into train and test
    train_set, test_set = split_dataset(all_datasets, num_input_signal, train_percentage)

    # train and predict
    score, scores, original, predictions, forecast_runtime, train_runtime, load_runtime, avg_forecast_times = \
        evaluate_model_cnn(train_set, test_set, num_input, num_output, num_input_signal, folder_path, name_model)

    # plot original and predicted dataset and save graphical comparison to image file.
    multi_plots(original, predictions, name_model, folder_path, score, num_channels)
