%% Use of envelop to generate time series data
close all;clear;
x=[0 0.25 0.5 0.75 1 0 1 0 1 0.25 0.25 0 0.25 0.25 0.25 0.25 0.5 1 0 0 0 0 ...
    1 0 1 0 1 0 1 1 1 1 1 1 1 0 0 0 0 0 0 0.25 0.25 0.25 0.25 0 0 0 0 0.75 ...
    1 1 1 1 1 1 1 0 0 1 0 1 0 0 1 0 0.25 0 0 0 0 0 0.25 0.5 0 0 0.5 0.25 ...
    0.75 0.25 0 0 0 0 0.75 0.75 0.75 0.75 0 1 0 1 0 1 0 0 0 0 0 0.25...
    0 0 0 0 0 0]
np=5;
ye=envelope(x,np,'peak');
[yeu,yel]=envelope(x,np);
% ye=envelope(x);
yh=abs(hilbert(x));
% q=quantizer;
% q = quantizer('fixed', 'ceil', 'saturate', [6 6])
% yq=quantize(q,ye);

stem(x,'r');
hold;grid;
plot(ye,'b');
% figure(2);
% stem(x,'r');
% hold;grid;
% plot(yeh,'b');