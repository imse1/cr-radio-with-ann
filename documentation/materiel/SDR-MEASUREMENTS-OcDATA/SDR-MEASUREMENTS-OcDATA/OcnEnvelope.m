%% Use of envelop with measured data of channel occupancy
%% JMR Feb. 8th/2022
close all;clear;
load Oc_19_ch_Fs_1.mat;
% np=70;
np=70;
ye=envelope(Ocn(1,:),np,'peak');
stem(Ocn(1,:),'r');
hold;grid;
%plot(ye,'b');
%plot(abs(ye),'kx');
%% Data Quantization
partition = [0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 ...
    0.7 0.75 0.8 0.85 0.9 0.95 1];
codebook=[0 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 ...
    0.7 0.75 0.8 0.85 0.9 0.95 1];
[indexq,yq]=quantiz(ye,partition,codebook);
plot(yq,'b-x');

