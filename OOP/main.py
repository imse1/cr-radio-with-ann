import numpy as np
import json
from scipy.interpolate import CubicSpline
from scipy.signal import butter,filtfilt
import adi
import time
import matplotlib.pyplot as plt
import tensorflow as tf
from keras import layers
import keras
import keras_utils
import copy
import os
import argparse


def decision(prediction,max_ampl,delay,trigger):
    output=[]
    last_channel=0
    new_channel=0
    number_above=0
    for i in range (len(prediction[0])):
        if(prediction[last_channel][i]>trigger*max_ampl):
            value_channels=[prediction[j][i] for j in range(len(prediction))]
            max_channel=np.array(value_channels).argmin()
            if(max_channel!=last_channel and (prediction[last_channel][i]-prediction[max_channel][i])>0.2):
                if(max_channel==new_channel):
                    number_above=number_above+1
                else:
                    new_channel=max_channel
                    number_above=0
                if(number_above>delay):    
                    last_channel=max_channel
                    number_above=0
        else:
            delay=0
        output.append(last_channel)
    return(output)

class npEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.int32):
            return int(obj)
        return json.JSONEncoder.default(self, obj)

class Occupancy():
    def __init__(self, n_channel=1, n=0, data=[], time=[], frequencies=[]):
        # Initialize the Occupancy object with provided or default values
        if len(time) == 0:
            time = np.arange(0, n, 1).astype(np.float64)
        if len(data) == 0:
            data = np.empty((n_channel, n))
        if len(frequencies) == 0:
            frequencies = np.array([0 for i in range(n_channel)])

        self.data = data
        self.time = time
        self.length = n
        self.channel = n_channel
        self.frequencies = frequencies

    # Getter method for returning occupancy data as a dictionary
    def __get__(self):
        return {
            "data": self.data, 
            "time": self.data, 
            "length": self.length, 
            "channel": self.channel, 
            "frequencies": self.frequencies
        }

    # Getter methods
    def get_length(self):
        return self.length

    def get_n_channel(self):
        return self.channel
    
    # Setter method for changing the number of channels
    def _set_n_channel(self, value):
        self.channel = value
    
    def get_frequencies(self):
        return self.frequencies

    # Setter methods for frequencies
    def set_frequencies(self, values):
        if len(values) != self.channel:
            raise ValueError("frequencies list must be same size as number of channels")
        self.frequencies = values

    def set_frequencies_linear(self, start, stop):
        self.frequencies = np.linspace(start, stop, self.channel)

    # Getter methods for time and related operations
    def get_time(self):
        return self.time
    
    def get_time_position(self, value: int, size=1):
        if value < 0 or value + size > self.length:
            raise ValueError("Value of position is out of time array")
        return self.time[value:value + size]
    
    # Find the position in time array closest to the given time
    def find_position_time(self, time):
        if time < self.time[0] or time >= self.time[-1]:
            raise ValueError("Requested time is out of time array")
        return (np.abs(self.time - time)).argmin()

    # Setter methods for time
    def set_time(self, values: np.ndarray):
        if len(values) != self.length:
            raise ValueError("Time array must be same size as occupancy length")
        self.time = values

    def set_time_linear(self, start, stop):
        self.time = np.linspace(start, stop, self.length)

    # Set specific time values at given indices
    def set_time_part(self, idx, values):
        if len(idx) != len(values) or min(idx) < 0 or max(idx) >= self.length:
            raise ValueError("Problem in values of idx or idx shape")
        self.time[idx] = values

    # Getter methods for data
    def get_data(self):
        return self.data

    # Get data for a specific channel
    def get_data_channel(self, channel: int):
        if channel >= self.channel or channel < 0:
            raise ValueError("Value of channel is out of channel list")
        return self.data[channel]

    def get_data_position(self, position: int, size=1):
        if position < -self.length or position + size > self.length:
            raise ValueError("Value of position is out of channel list")
        return self.data[:, position:position + size]
    
    # Get data for a specific time
    def get_data_with_time(self, time):
        idx = self.find_position_time(time)
        return self.data[:, idx]

    # Setter methods for data
    def set_data(self, values):
        if np.shape(self.data) != np.shape(values):
            raise ValueError("Data must have same size as current occupancy data")
        self.data = values

    # Set data for a specific range of indices
    def set_data_part(self, idx_start: int, data):
        if len(self.data) != len(data):
            raise ValueError("Data shape dim 1 must be the same as number of channels")
        if idx_start < 0 or idx_start >= self.length:
            raise ValueError("Error in idx value")
        idx = [i + idx_start for i in range(len(data[0]))]
        self.data[:, idx] = data

    # Add new occupancy data to the existing data
    def add_occupancy_value(self, data, time=[]):
        if len(time) == 0:
            time = [self.time[-1] + 1 + i for i in range(np.shape(data)[1])]
        if len(data) != self.channel:
            raise ValueError("New data must be same length as number of channels")
        self.data = np.append(self.data, data, axis=1)
        self.time = np.append(self.time, time)
        self.length = self.length + np.shape(data)[1]

    # Add another Occupancy object's data to the current object
    def add(self, occupancy):
        if self.channel != occupancy.channel or not (self.frequencies == occupancy.frequencies).all():
            raise ValueError("Input class must use same frequencies and have same number of channels")
        data = np.empty((self.channel, self.length + occupancy.length))
        data[:, :self.length] = self.data
        data[:, self.length:] = occupancy.data
        self.data = data
        time = np.empty((self.length + occupancy.length))
        time[:self.length] = self.time
        time[self.length:] = occupancy.time
        self.time = time
        self.length = self.length + occupancy.length

    # Save occupancy data to a text file
    def save_txt(self, path, name):
        occ = {
            "data": self.data.tolist(),
            "time": self.time.tolist(),
            "frequencies": self.frequencies.tolist(),
            "length": self.length,
            "channel": self.channel
        }
        with open(path + name + '.txt', 'w') as outfile:
            json.dump(occ, outfile)

    # Load occupancy data from a text file
    def load_txt(self, path, name):
        with open(path + name + '.txt', 'r') as infile:
            occ = json.load(infile)
        self.data = np.array(occ["data"])
        self.time = np.array(occ["time"])
        self.frequencies = np.array(occ["frequencies"])
        self.length = int(occ["length"])
        self.channel = int(occ["channel"])

    # Create a copy of the Occupancy object
    def copy(self):
        return Occupancy(n_channel=self.channel, n=self.length, data=self.data, time=self.time, frequencies=self.frequencies)


    def filter(self, min_val, max_val):
        N = 4
        T = (self.time[-1] - self.time[0]) / (self.length * N)  # Sample Period
        fs = 1 / T  # Sample rate in Hz
        cutoff = 2  # Desired cutoff frequency of the filter in Hz
        nyq = 0.5 * fs  # Nyquist Frequency
        normal_cutoff = cutoff / nyq
        fOrder = 2
        b, a = butter(fOrder, normal_cutoff, btype='low')  # Design Butterworth filter
        idx = [i for i in range(N - 1, self.length * N, N)]  # Indices for downsampling
        
        # Apply envelope extraction, filtering, and normalization to each channel's data
        for x in range(self.channel):
            self.data[x] = compute_envelope(self.data[x], self.length * N)[idx]
            self.data[x] = filtfilt(b, a, self.data[x], padlen=fOrder * 3)
            self.data[x] = 100 * (self.data[x] - min_val) / (max_val - min_val)

        
    # Select a window of data within the occupancy object
    def window(self, id_start, size):
        if id_start < -self.length or id_start + size > self.length:
            raise ValueError("Window must fit within the length value")
        self.data = self.data[:, id_start:id_start + size]  # Update data within the window
        self.time = self.time[id_start:id_start + size]      # Update time within the window
        self.length = size                                    

    # Insert data from another Occupancy object into the current object
    def insert(self, Occ, start: int):
        if start + Occ.length >= self.length:
            raise ValueError("Insertion goes above parent length")
        if not (Occ.frequencies == self.frequencies).all():
            raise ValueError("Mismatch between linked frequencies")
        if Occ.channel != self.channel:
            raise ValueError("Number of channels must be the same")
        self.data[:, start:start + Occ.length] = Occ.data   # Insert data
        self.time[start:start + Occ.length] = Occ.time      # Update time within the insertion range

    # Gather occupancy data from a software-defined radio (SDR)
    def gather_data(self, sdr, start_time):
        fftfreqs = np.fft.fftfreq(sdr.rx_buffer_size, 1 / sdr.sample_rate) + ((self.frequencies[0] + self.frequencies[-1]) / 2)
        NFREQ = len(fftfreqs)
        idx = np.argsort(fftfreqs)
        fftfreqs = fftfreqs[idx]
        for i in range(self.length):

            # Receive samples from PlutoSDR
            data = sdr.rx()  # 'data' will contain the received samples in a complex array
            module = np.abs(data)
            ps = np.fft.fft(module)
            ps = np.abs(ps[idx])
            flog = np.log10(ps)
            flog = flog[idx]
            BP = np.empty((self.channel, 1))
            for j in range(self.channel):
                # Calculate the Band Power for each channel using FFT results
                BP[j] = [np.multiply(np.sum(flog[j * NFREQ // self.channel:(j + 1) * NFREQ // self.channel]), NFREQ // self.channel)]
            self.set_data_part(i, BP)            # Update data with calculated Band Power
            t = time.time()
            self.time[i] = t - start_time        # Update time

    # Plot the occupancy data for each channel over time
    def plot(self):
        for i in range(self.channel):
            plt.plot(self.time, self.data[i], '.-')
            plt.show()

    # Make predictions using a trained machine learning model
    def make_prediction(self, model, mapping, model_info, future_length):
        if model_info['n_channel'] != self.channel:
            raise ValueError("Occupancy must match model's number of channels")
        start_predict = time.time()
        t = self.time[-1]
        input_data = np.reshape(self.data[:, -model_info['n_input']:],
                                (self.channel, 1, model_info['n_input'], 1))   # Prepare input data for prediction
        for i in range(self.channel):
            # Set input tensor for the current channel
            model.set_tensor(mapping[i]['input_index'], input_data[i].astype(np.float32))
        model.invoke()  # Run the model
        output_tensor = np.empty((self.channel, 1, model_info['n_output']))
        for i in range(self.channel):
            # Get the prediction for the current channel
            output_tensor[i] = model.get_tensor(mapping[i]['output_index'])
        output_tensor = np.reshape(output_tensor, (self.channel, model_info['n_output']))
        end_predict = time.time()
        computation_time = end_predict - start_predict
        print("Computation time:", computation_time)
        time_array = np.linspace(t + computation_time, t + computation_time + future_length,
                                 model_info['n_output'])  # Generate time array for the prediction
        output = Occupancy(self.channel, model_info['n_output'])
        output.data = output_tensor
        output.length = model_info['n_output']
        output.frequencies = self.frequencies
        output.time = time_array
        return output

    # Display information about the occupancy object
    def show(self):
        print("data:", self.data)
        print("time:", self.time)
        print("length:", self.length)
        print("n_channel:", self.channel)
        print("frequencies:", self.frequencies)


class Selection():
    def __init__(self, amplitude, delay, trigger, time_delay):
        # Initialize the Selection object with specified parameters
        # and empty lists to store selected items and corresponding times.
        self.selected = []   # List to store selected indices
        self.time = []       # List to store corresponding times
        self.amplitude = amplitude   # Amplitude threshold for selection
        self.delay = delay           # Delay for extracting occupancy data
        self.trigger = trigger       # Trigger value for amplitude comparison
        self.time_delay = time_delay # Time delay for validating conditions

    def add(self, Occupancy: Occupancy, start_time):
        # Add a new item to the selection based on occupancy data and time
        if Occupancy.length > self.delay:
            lastOcc = Occupancy.data[:, -(self.delay+1):]
        else:
            lastOcc = Occupancy.data
        
        # Calculate the average values for each occupancy data set
        avg = np.mean(lastOcc, axis=1)
        print("moyenne : ",avg)
        # Find the index of the minimum average value
        id_min = np.argmin(avg)

        if len(self.selected) > 0:
            if (self.time[-1] - self.time[0]) > self.time_delay:
                # Check if the condition for time delay validation is met
                idx_time = (np.abs(np.array(self.time) - (self.time[-1] - self.time_delay))).argmin()
                if np.all(self.selected[idx_time:] == self.selected[-1]):
                    # Validate the condition and update selected based on amplitude
                    old_selected = self.selected[-1]
                    if avg[id_min] < (avg[old_selected] - (self.amplitude * self.trigger)):
                        self.selected.append(id_min)
                    else:
                        self.selected.append(self.selected[-1])
                else:
                    # Condition not met, retain the previous selection
                    self.selected.append(self.selected[-1])
            else:
                # Update selected based on amplitude without time delay validation
                old_selected = self.selected[-1]
                if avg[id_min] < (avg[old_selected] - (self.amplitude * self.trigger)):
                    self.selected.append(id_min)
                else:
                    self.selected.append(self.selected[-1])
        else:
            # Initial selection if the list is empty
            self.selected.append(id_min)
        
        # Add the corresponding time to the time list
        self.time.append(time.time() - start_time)
        print("new selected  : ", self.selected[-1])

    def plot(self):
        # Plot the selected indices over time
        plt.plot(self.time, self.selected)
        plt.show()

       
def create_dataset(model_info,size,computation_time,future_length,sdr,start_time,min,max):
    """
    Create an occupancy dataset based on the provided parameters.

    Args:
        model_info (dict): Information about the model and data.
        size (int): Number of samples in the dataset.
        computation_time (float): Computation time for each sample.
        future_length (float): Time interval for predicting future occupancy.
        sdr (object): Software-defined radio object.
        start_time (float): Starting time for data gathering.
        min (float): Minimum value for data filtering.
        max (float): Maximum value for data filtering.

    Returns:
        list[dict]: A list of dictionaries, each containing input and output occupancy data.
    """
    # Calculate frequency values based on SDR parameters.
    freq=np.array([(sdr.rx_lo+sdr.rx_rf_bandwidth*((1/model_info['n_channel'])-1)/2)+i*sdr.rx_rf_bandwidth/model_info['n_channel'] for i in range (model_info['n_channel'])])
    print("frequence : ",freq)
    dataset=[]
    print("dataset generation")
    occ=Occupancy(model_info['n_channel'],(model_info['n_input']+model_info['n_output'])+size+int((computation_time+future_length)*800),frequencies=freq)
    
    t1=time.time()
    occ.gather_data(sdr,start_time)
    occ.filter(min,max)
    print("first part ended")
    for i in range(size):
        if((i*100/size)%1==0):
            print(int(i*100/size),"%")
        in_occ=copy.deepcopy(occ)
        in_occ.window(i,model_info['n_input'])
        tFinIn=in_occ.time[-1]
        out_occ=Occupancy(model_info['n_channel'],model_info['n_output'],frequencies=freq)
        for j in range(model_info['n_output']):
            indice=occ.find_position_time(tFinIn+computation_time+(j*future_length/model_info['n_output']))
            out_occ.data[:,j]=occ.data[:,indice]
            out_occ.time[j]=occ.time[indice]
        dataset.append({"input": in_occ, "output": out_occ})
    t2=time.time()
    print("time gathering : ",t2-t1,"s")
        
    
    return(dataset)


def save_dataset(dataset,path,name):
    """
    Save an occupancy dataset to a JSON file.

    Args:
        dataset (list[dict]): List of dictionaries containing input and output occupancy data.
        path (str): Path to the directory for saving the dataset.
        name (str): Name of the dataset file (without extension).
    """
    with open(path+name+'.txt', 'w') as outfile:
        tot_ds=[]
        print("len dataset : ", len(dataset))
        for i in range (len(dataset)):

            ds={"input":{
                    "data": dataset[i]['input'].data.tolist(),
                    "time": dataset[i]['input'].time.tolist(),
                    "frequencies": dataset[i]['input'].frequencies.tolist(),
                    "length": dataset[i]['input'].length,
                    "channel": dataset[i]['input'].channel},
                "output": {
                    "data": dataset[i]['output'].data.tolist(),
                    "time": dataset[i]['output'].time.tolist(),
                    "frequencies": dataset[i]['output'].frequencies.tolist(),
                    "length": dataset[i]['output'].length,
                    "channel": dataset[i]['output'].channel}}     
            tot_ds.append(ds)    
        json.dump(tot_ds, outfile, cls=npEncoder)
           

def load_dataset(path,name):
    """
    Load an occupancy dataset from a JSON file.

    Args:
        path (str): Path to the directory containing the dataset.
        name (str): Name of the dataset file (without extension).

    Returns:
        list[dict]: List of dictionaries containing input and output occupancy data.
    """
    with open(path+name+'.txt', 'r') as infile:
        occ=json.load(infile)
    dataset=[]
    for i in range(len(occ)):
        in_occ=occ[i]["input"]
        input=Occupancy(n_channel=in_occ["channel"],n=in_occ["length"],data=in_occ["data"],time=in_occ["time"],frequencies=in_occ["frequencies"])
        out_occ=occ[i]["output"]
        output=Occupancy(n_channel=out_occ["channel"],n=out_occ["length"],data=out_occ["data"],time=out_occ["time"],frequencies=out_occ["frequencies"])
        dataset.append({"input": input, "output": output})
    return(dataset)

def get_dataset_input_output(dataset):
    """
    Extract and organize input and output data from the dataset.

    Args:
        dataset (list[dict]): List of dictionaries containing input and output occupancy data.

    Returns:
        tuple: A tuple containing two numpy arrays: (input_data, output_data)
    """
    in_dim1=len(dataset)
    in_dim2=dataset[0]["input"].channel
    in_dim3=dataset[0]["input"].length
    out_dim1=in_dim1
    out_dim2=dataset[0]["output"].channel
    out_dim3=dataset[0]["output"].length
    input=np.empty((in_dim1,in_dim2,in_dim3))
    output=np.empty((out_dim1,out_dim2,out_dim3))
    for i in range(in_dim1):
        dataset[i]["input"].show()
        input[i]=dataset[i]["input"].data
        output[i]=dataset[i]["output"].data
    return(input,output)

def compute_envelope(signal, num_points):
    """
    Compute the envelope of a signal using cubic spline interpolation.

    Args:
        signal (numpy.ndarray): Input signal to compute the envelope for.
        num_points (int): Number of points in the resulting envelope.

    Returns:
        numpy.ndarray: The envelope of the input signal with the specified number of points.
    """
    x = np.arange(len(signal))
    cs = CubicSpline(x, signal)
    new_x = np.linspace(0, len(signal) - 1, num_points)
    return cs(new_x)

def create_signature(n_channel):
    """
    Create a list of channel signatures based on the number of channels.

    Args:
        n_channel (int): Number of channels.

    Returns:
        list: A list of channel signatures, where each signature is a combination of alphabetic characters.
    """
    alpha=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    signature=[]
    for i in range(n_channel):
        signature.append(alpha[i//26]+alpha[i%26])
    return(signature)

def find_input_output_order(interpreter,n_channel):
    """
    Find the mapping of input and output indices for a TensorFlow Lite interpreter.

    Args:
        interpreter: TensorFlow Lite interpreter.
        n_channel (int): Number of channels.

    Returns:
        list[dict]: A list of dictionaries containing input and output indices mapping for each channel.
    """
    mapping = [{'input_index': None, 'output_index': None} for _ in range(n_channel)]
    signatures=create_signature(n_channel)
    input_details=interpreter.get_input_details()
    output_details=interpreter.get_output_details()
    min=9999
    for i in range(len(output_details)):
        if output_details[i]['index']<min:
            min=output_details[i]['index']
    for i in range(n_channel):
        q=0
        while(signatures[i]!=input_details[q]['name'][-4:-2]):
            q=q+1
        mapping[i]={'input_index':input_details[q]['index'] , 'output_index':min+i}  
    return mapping

def load_tflite_model(tflite_model_path):
    """
    Load a TensorFlow Lite model and gather information about its input, output, and channel mapping.

    Args:
        tflite_model_path (str): Path to the TensorFlow Lite model file.

    Returns:
        tuple: A tuple containing the TensorFlow Lite interpreter, channel mapping, and model information.
    """
    interpreter = tf.lite.Interpreter(model_path=tflite_model_path)
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    model_info={'n_channel': len(input_details) , 'n_input' : input_details[0]['shape'][1] , 'n_output' : output_details[0]['shape'][1]}
    n_channels=len(input_details)
    mapping=find_input_output_order(interpreter,n_channels)
    return(interpreter,mapping,model_info)

def setSDR(sample_rate,num_samples,gain,bandwidth,center_freq):
    """
    Configure and initialize the SDR (Software-Defined Radio) settings.

    Args:
        sample_rate (float): Sample rate in Hz.
        num_samples (int): Number of samples in the RX buffer.
        gain (float): RX gain in dB.
        bandwidth (float): RX RF bandwidth in Hz.
        center_freq (float): RX center frequency in Hz.

    Returns:
        object: An instance of the SDR with the specified settings.
    """
    sdr = adi.Pluto()
    sdr.sample_rate = int(sample_rate)
    sdr.rx_buffer_size = num_samples  # this is the buffer the Pluto uses to buffer samples
    sdr.gain_control_mode_chan0 = 'manual'
    sdr.rx_hardwaregain_chan0 = gain  # dB. Allowable range is 0 to 74.5 dB
    sdr.rx_rf_bandwidth = int(bandwidth)
    sdr.rx_lo = int(center_freq)
    return(sdr)

def savehd5(model, folder_path):
    """
    Save a Keras model to HDF5 format along with its weights.

    Args:
        model (tensorflow.keras.Model): The Keras model to be saved.
        folder_path (str): Path to the directory for saving the model.
    """
    # serialize model to JSON
    model_json = model.to_json()
    with open(folder_path + "model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(folder_path + "model.h5")
    print("Saved model to disk")

def convert_to_tflite(keras_model, tflite_model_path,name):
    """
    Convert a Keras model to TensorFlow Lite format and save it.

    Args:
        keras_model (tensorflow.keras.Model): The Keras model to be converted.
        tflite_model_path (str): Path to the directory for saving the TensorFlow Lite model.
        name (str): Name of the TensorFlow Lite model file.
    """
    print('----> Start of conversion to TFLite <----')
    # Convert the Keras model to a TensorFlow Lite model
    converter = tf.lite.TFLiteConverter.from_keras_model(keras_model)
    

    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model = converter.convert()

    # Save the TensorFlow Lite model to a file
    with open(tflite_model_path+name, 'wb') as f:
        f.write(tflite_model)
    print('----> End of conversion <----')
    print(f"TensorFlow Lite model saved to: {tflite_model_path+name}")
    return(tflite_model)


def train_keras_model(model,dataset,train_param,n_channel):
    """
    Train a Keras model using the provided dataset.

    Args:
        model (tensorflow.keras.Model): The Keras model to be trained.
        dataset (list[dict]): List of dictionaries containing input and output occupancy data.
        train_param (dict): Parameters for training the model.
        n_channel (int): Number of channels.

    Returns:
        tensorflow.keras.Model: The trained Keras model.
    """
    input,output=get_dataset_input_output(dataset)
    (i1,i2,i3)=np.shape(input)
    (o1,o2,o3)=np.shape(output)
    inp1=np.reshape(input,(i2,i1,i3))
    out1=np.reshape(output,(o2,o1,o3))
    inp=[inp1[i] for i in range(i2)]
    out=[out1[i] for i in range(o2)]
    model.fit(inp, out, epochs=train_param['epochs'],
            batch_size=14, verbose=['verbose'])
    return(model)

def create_model(dataset,path,model_name,train_param):
    """
    Create, compile, and train a Keras model based on the provided dataset and parameters.

    Args:
        dataset (list[dict]): List of dictionaries containing input and output occupancy data.
        path (str): Path to the directory for saving the model.
        model_name (str): Name of the model files.
        train_param (dict): Parameters for training the model.
    """
    n_channels,n_inputs,n_outputs=dataset[0]["input"].channel,dataset[0]["input"].length,dataset[0]["output"].length
    model_output = np.empty(n_channels, dtype=object)
    model_input = np.empty(n_channels, dtype=object)
    letters=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    for q in range(0, n_channels):
        name_channel = 'channel_' + letters[q//26] + letters[q%26]
        # define input node
        channel_input = keras.Input(shape=(n_inputs,1), name=name_channel)
        model_input[q] = channel_input
    # Convert array to list
    input_list = model_input.tolist()
    
    layer_1_1=[]
    layer_1_2=[]
    layer_1_3=[]
    layer_1_5=[]
    for i in range(n_channels):
        layer_1_1.append(layers.Conv1D(filters=64, kernel_size=3,activation='relu')(model_input[i]))
        # adding a second hidden layer
        layer_1_2.append(layers.MaxPooling1D(pool_size=2)(layer_1_1[i]))
        # adding a third hidden layer
        layer_1_3.append(layers.Flatten()(layer_1_2[i]))
    # adding a fourth hidden layer
    #layer_1_4 = layers.Concatenate()(layer_1_3)
    # adding a fifth hidden layer
        #layer_1_5.append(tfmot.quantization.keras.quantize_annotate_layer(layers.Dense(n_output*3, activation='relu'))(layer_1_3[i]))
        layer_1_5.append(layers.Dense(n_outputs*3, activation='relu')(layer_1_3[i]))
    #layer_1_5 = layers.Dense(n_output*n_channels*4, activation='relu')(layer_1_3)

        name_channel = 'out_channel_' + letters[i//26] + letters[i%26]
        # adding the output layer
        output_1 = layers.Dense(n_outputs, activation='linear', name=name_channel)(layer_1_5[i])    
        model_output[i] = output_1

    # **************************************** Instantiate Input and Outputs ***************************************** #
    # Convert arrays to list
    output_list = model_output.tolist()

    model = keras.Model(inputs=input_list,
                        outputs=output_list, name='channels_models')

    # ******************************************** Compiling both inputs ********************************************* #

    # compiling the neural network
    model.compile(loss='mse', optimizer='adam', metrics=['acc', 'mse'])
    tf.keras.utils.plot_model(model,"data/model.png",show_shapes=True)
    model=train_keras_model(model,dataset,train_param,n_channels)

    savehd5(model, path)
    convert_to_tflite(model, path,model_name)

def computeRMSE(input: Occupancy,forecast: Occupancy):
    """
    Compute the Root Mean Squared Error (RMSE) between input and forecast occupancy data.

    Args:
        input (Occupancy): The input occupancy data.
        forecast (Occupancy): The forecasted occupancy data.

    Returns:
        numpy.ndarray: Array of RMSE values for each channel.
    """
    clean_in=[]
    clean_out=[]
    for i in range (forecast.length):
        dataf=forecast.get_data_position(i)
        time=forecast.get_time_position(i)
        try:
            indice=input.find_position_time(time)
            datai=input.get_data_position(indice)
            clean_in.append(datai)
            clean_out.append(dataf)
        except:
            j=0
    clean_in=np.array(clean_in)
    clean_out=np.array(clean_out)
    max=np.max(clean_out)
    error=np.subtract(clean_out,clean_in)
    rmse=np.empty(forecast.channel)
    for i in range(forecast.channel):
        rmse[i]=np.divide(np.sqrt(np.mean(np.square(error[i]))),max)
    return(np.round(rmse,4))


def main(center_freq, sample_rate, PRECISION, bandwidth, epoch, model_path, computation_time, future_length, Sdelay, Strigger, Stime_delay,
         n_input, n_output, n_channel, verbose, create, length_dataset, n_run):
    """
    The main function that orchestrates the data collection, training, prediction, and visualization process.

    Args:
        center_freq (float): Center frequency for the SDR.
        sample_rate (float): Sample rate for data collection.
        PRECISION (int): Precision parameter for data collection.
        bandwidth (float): RF bandwidth for the SDR.
        epoch (int): Number of epochs for model training.
        model_path (str): Path for saving and loading model files.
        computation_time (float): Time interval for computation.
        future_length (float): Time interval for future occupancy prediction.
        Sdelay (int): Delay for the selection process.
        Strigger (float): Trigger threshold for the selection process.
        Stime_delay (float): Time delay for the selection process.
        n_input (int): Number of input channels for the model.
        n_output (int): Number of output channels for the model.
        n_channel (int): Total number of channels.
        verbose (int): Verbosity level for model training.
        create (bool): Flag indicating whether to create a new dataset.
        length_dataset (int): Length of the dataset.
        n_run (int): Number of runs.

    Returns:
        None
    """
    
    # SDR parameters
    num_samples = 1024 * PRECISION  # Number of samples to read in each iteration
    gain = 20  # dB  # Gain setting for the SDR

    # Define training parameters and model information
    train_param = {'epochs': epoch, 'batch_size': 1, 'verbose': 2}
    model_info = {"n_input": n_input, "n_output": n_output, "n_channel": n_channel}

    # Connect to the PlutoSDR device and configure settings
    sdr = setSDR(sample_rate, num_samples, gain, bandwidth, center_freq)

    # Gather limits and calculate data range
    start = time.time()
    limits = Occupancy(model_info["n_channel"], 1000)
    limits.gather_data(sdr, start)
    min_value = np.min(limits.data) * 0.9
    max_value = np.max(limits.data) * 1.1

    # If dataset creation is requested, generate, save, and load the dataset
    if create:
        dataset = create_dataset(model_info, length_dataset, computation_time, future_length, sdr, time.time(), min_value, max_value)
        save_dataset(dataset, model_path, "dataset")
        dataset = load_dataset(model_path, "dataset")
        create_model(dataset, model_path, "tflite_model", train_param)

    # Load the TensorFlow Lite model and prepare for data gathering and prediction
    (model, mapping, model_info) = load_tflite_model(model_path + "tflite_model")
    gathered = Occupancy(model_info["n_channel"], 1000)
    prediction = Occupancy(model_info["n_channel"])
    selecteur = Selection(100, Sdelay, Strigger, Stime_delay)
    gathered.gather_data(sdr, start)

    # Run data collection, prediction, and selection for n_run times
    for i in range(n_run):
        print("run : ", i)
        input = Occupancy(model_info["n_channel"], model_info['n_input'], frequencies=gathered.frequencies)
        input.gather_data(sdr, start)
        gathered.add(input)
        filtered = copy.deepcopy(gathered)
        filtered.filter(min_value, max_value)
        output = filtered.make_prediction(model, mapping, model_info, future_length)
        prediction.add(output)
        selecteur.add(prediction, start)

    # Compute and print RMSE values for each channel
    rmse = computeRMSE(filtered, prediction)
    print(rmse)

    # Plot selection process and channel data
    selecteur.plot()
    for i in range(input.channel):
        plt.plot(filtered.time, filtered.data[i, :], color='b')
        plt.plot(prediction.time, prediction.data[i, :], color="r")
        plt.title('RMSE channel' + str(i) + ': ' + str(rmse[i]))
        plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Program inputs.')
    parser.add_argument('-CF', '--center-freq', type=float,
                        help='center frequencie to use, default=2.5e9', default=2.5e9, required=False)
    parser.add_argument('-SR', '--sample-rate', type=int,
                        help='sample rate between SDR and computer, default=60e6', default=60e6, required=False)
    parser.add_argument('-PR', '--precision', type=int,
                        help='number of sample per call 1024*precision, default=5', default=5, required=False)
    parser.add_argument('-BW', '--bandwidth', type=float,
                        help='bandwidth to use, default=70e6', default=70e6, required=False)
    parser.add_argument('-E', '--epoch', type=int,
                        help='number of epoch to do, default=30', default=30, required=False)
    parser.add_argument('-P', '--model-path', type=str,
                        help='model path, default="data2/"', default="data2/", required=False)
    parser.add_argument('-CT', '--computation-time', type=float,
                        help='computation time used for dataset creation, default=0.1', default=0.1, required=False)
    parser.add_argument('-FL', '--future-length', type=float,
                        help='length of the forecast used, default=0.05', default=0.05, required=False)
    parser.add_argument('-SD', '--selector-delay', type=int,
                        help='number of time step in the mean, default=0 ', default=0, required=False)
    parser.add_argument('-ST', '--selector-trigger', type=float,
                        help='value in precent under last selected to reach before switching, default=0', default=0, required=False)
    parser.add_argument('-STD', '--selector-time-delay', type=float,
                        help='time to wait between 2 channels switch in s, default=0', default=0, required=False)
    parser.add_argument('-NI', '--n-input', type=int,
                        help='number of input when creating model, default=10', default=10, required=False)
    parser.add_argument('-NI0', '--n-output', type=int,
                        help='number of output when creating model, default=5', default=5, required=False)
    parser.add_argument('-NC', '--n-channel', type=int,
                        help='number of channel when creating model, default=10', default=10, required=False)
    parser.add_argument('-LD', '--length-dataset', type=int,
                        help='length of dataset when creating a new model, default=5000', default=5000, required=False)
    parser.add_argument('-NR', '--n-run', type=int,
                        help='number of run to do, default=500', default=500, required=False)
    parser.add_argument('-V', '--verbose',
                        help='debeug mode', action='store_true',)
    parser.add_argument('-C', '--create',
                        help='use to create and store a new model', action='store_true',)

    args = parser.parse_args()
    main(
        args.center_freq,args.sample_rate,args.precision,args.bandwidth,args.epoch,args.model_path,args.computation_time,args.future_length,
        args.selector_delay,args.selector_trigger,args.selector_time_delay,args.n_input,args.n_output,args.n_channel,args.verbose, args.create, args.length_dataset, args.n_run
    )
