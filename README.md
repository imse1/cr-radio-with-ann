# Occupancy Prediction and Selection with SDR and TensorFlow Lite

This repository contains a Python script that enables occupancy prediction and selection using Software-Defined Radio (SDR) data and TensorFlow Lite models. The script includes functionalities for data collection, dataset creation, model training, prediction, and selection, as well as visualization of results.


# OOP folder

## Usage
This python script is a POC to show how it is possible to dynamically select a channel to use for a CR system

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Python (version >= 3.7) is installed on your system.
- Required Python packages are installed. You can install them using the provided `requirements.txt` file:
- Instalation tutorial for the required Pluto SDR plug in can be found [here](https://pysdr.org/content/pluto.html). Following Installing PlutoSDR Driver section should work
-Please not that this project only work on linux based OS. Adaptating it for Windows or MacOS is possible but it hasen't been done.

## First run
Once all the Prerequisites are installed and the SDR plugged in, go to `OOP` folder and type `python3 main.py`. Everything should work fine. A list of tunable parameters can be foud by using `python3 main.py --help`
# troubleshot
The most common error that you may encounter is due to the function `setSDR`, at line `sdr = adi.Pluto('ip:192.168.50.85')`. If you face this error you need either to replace it by `sdr = adi.Pluto()` or replace the IP by the one you can find in the `plutoSDR` drive, file `config.txt`, line `ip_addr`. You can find a demo on how to change it in this repo: `video_presentation/config_IP`. I don't know why, sometimes it's one and sometimes the other ¯|_(ツ)_/¯

## How to Use

1. **Clone the Repository:**
git clone git@gitlab.com:imse1/cr-radio-with-ann.git

2. **Configure SDR Parameters:**
Open the `main.py` file and locate the section labeled `SDR parameters`. Modify the following parameters to match your SDR device's specifications:
- `center_freq`: Center frequency for the SDR.
- `sample_rate`: Sample rate for data collection.
- `PRECISION`: Precision parameter for data collection.
- `bandwidth`: RF bandwidth for the SDR.

3. **Configure Training and Model Parameters:**
Adjust the parameters in the `main` function to customize your training and prediction process. These include:
- `epoch`: Number of epochs for model training.
- `model_path`: Path for saving and loading model files.
- `computation_time`: Time interval for computation.
- `future_length`: Time interval for future occupancy prediction.
- `Sdelay`: Delay for the selection process.
- `Strigger`: Trigger threshold for the selection process.
- `Stime_delay`: Time delay for the selection process.
- `n_input`: Number of input channels for the model.
- `n_output`: Number of output channels for the model.
- `n_channel`: Total number of channels.
- `verbose`: Verbosity level for model training.
- `create`: Flag indicating whether to create a new dataset.
- `length_dataset`: Length of the dataset.
- `n_run`: Number of runs.

4. **Run the Script:**
Run the modified `main.py` script to execute the occupancy prediction and selection process:


5. **Visualization:**
The script will generate plots showing the selection process and channel data. Review the generated plots to analyze the results.

## Modifying the Code

To modify the code for your specific use case, consider the following steps:

- **Custom Model Architecture:** If you want to use a different model architecture or modify the existing one, you can do so by modifying the `create_model` function in the `main.py` script.

- **Data Gathering:** If you need to get data differently, modify the `gather_data` methd and related functions as needed.

- **Data Preprocessing:** If you need to preprocess data differently, modify the `compute_envelope` function or `filter` methd and related functions as needed.

- **Channel Selection:** If you want to changes the way a channel is selected, you can acheive it by modifyig the methods from the `Selection` class

- **Additional Features:** You can extend the code to include additional features or functionalities based on your requirements. Add new functions or modify existing ones as necessary.

## Conclusion

This repository provides a framework for occupancy prediction and selection using SDR and TensorFlow Lite. By following the instructions above, you can easily use the code for your own occupancy prediction projects and customize it according to your needs.

For any questions or issues, please feel free to create an issue in the repository.

Happy occupancy prediction!

# Embedd scripts
The following instructions are related to the `main_OSTR.py` file, but can easily be transleted to `main_real.py`, as these thwo script does nearly the same.
The main difference is the fact that `main_OSTR.py` use real time consideration and multi threading.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Setup](#setup)
- [Modification](#modification)

## Introduction

This script showcases the integration of machine learning, signal processing, and real-time data acquisition. It includes three main components:
- **PlutoSDR Data Collection:** Acquires raw data from the PlutoSDR device, filters it, and prepares it for prediction.
- **Convolutional Neural Network (CNN):** Utilizes a pre-trained model (Keras or TensorFlow Lite) to predict outcomes based on input data.
- **Decision Logic:** Applies decision-making algorithms to select the most relevant outcomes and logs the results.

It uses real time optimisation with shared ressources to allow fast desicion during it's run.
The desision isn't linked to a defined time period, this script is meant to be considered as a proof of concept for the developpenment of a cognitive radio system.

## Features

- **Real-time Data Processing:** Collects data from the PlutoSDR device, filters it, and feeds it to a CNN for real-time predictions.
- **Modular Design:** The code is organized into distinct functions, making it easy to understand, maintain, and extend.
- **Threaded Execution:** Utilizes multithreading for efficient parallel processing of data collection, prediction, and decision-making.
- **Configurability:** Adjustable parameters allow customization of the data collection, prediction, and decision processes.
- **Data Logging:** Logs processed data and channel selection results for analysis and evaluation.

## Setup

1. Clone this repository to your local machine:
   `git clone git@gitlab.com:imse1/cr-radio-with-ann.git`

2. Install the required Python dependencies:
    `pip install -r requirements.txt`

3. Navigate to the right folder: 
    `cd embed`

4. Run the main Python script with the desired arguments:
    `python main.py -P <path_to_model> -MN <model_name> -LM <load_method> -FM <forecast_method> -NR <num_runs> -V`

## Modification

To adapt the IMSE system for your own projects, consider the following steps:

- **Custom Model:** Replace or fine-tune the pre-trained CNN model (imse.make_prediction) to suit your specific use case.

- **Data Source:** Modify the data collection logic (PlutoSDR) to interface with your data source or sensors.

- **Decision Logic:** Update the decision-making algorithm (Decide) based on the desired outcome and criteria.

- **Data Logging:** Adjust the data logging mechanism (imse.write_data) to save processed data and results in a suitable format.

- **Configuration Parameters:** Modify the configuration parameters in the main function to fit your project requirements.

# Multi light optimised
This repository contains Python code that performs simulation data generation, analysis, and forecasting using a convolutional neural network (CNN) model. The code processes simulation data, trains a CNN model, and predicts future data points. The generated model's performance is evaluated, and graphical comparisons between the original and predicted datasets are provided.
This script is meant to be used to create, train and evaluate models in order to use them in other scripts.

## Features

- **Data Generation:** The code generates simulation data based on specified parameters, including the simulation number, number of channels, training percentage, number of input samples, and number of output samples.

-    **CNN Model Training:** A CNN model is trained using the generated dataset to forecast future data points.

-    **Performance Evaluation:** The trained model's performance is evaluated, including prediction scores and runtime statistics.

-    **Graphical Comparison:** Graphs comparing the original and predicted datasets are generated and saved as image files.

-    **Decision Making:** The code includes a decision-making process based on amplitude, delay, and trigger values applied to the predicted data.

## Setup

1. Clone this repository to your local machine:
   `git clone git@gitlab.com:imse1/cr-radio-with-ann.git`

2. Install the required Python dependencies:
    `pip install -r requirements.txt`

3. Navigate to the right folder: 
    `cd multi_lite_optimised`

4. Run the script:
    `python3 multi.py`
A test dataset is given but it can be modified later.

## Usage
1. Place your data used for model training and validation at the same level as `multy.py` and name it `data.txt`. If you want to create a dataset, you can do it using the `pluto.py` script. You can tune you dataset shape in the initialisation part.

2. Open the `multi.py` file.

3. Modify the following variables according to your requirements:

-    SIMULATION_NUMBER: The simulation number.
-    num_channels: The number of channels in the simulation.
-    train_percentage: The percentage of data to be used for training.
-    num_input: The number of input samples.
-    num_output: The number of output samples.
-    folder_path: The folder path to save data and results.
-    load_model: Set to True if you want to load a pre-existing model, False otherwise.
-    name_model: The name of the model.
-    model_number: The model number.
-    Adjust other parameters as needed.

4. Run the script: 
`python3 multi.py`

5. The script will perform data generation, model training, prediction, evaluation, and decision making. Graphical comparisons and forecast time information will be displayed. The created model will be saved in the desired folder.

## Modifying the code
You can modify the code to suit your specific needs:

-   Adjust the data generation process by modifying the read_data function.
-   Experiment with different model architectures by modifying the evaluate_model_cnn function.
-   Customize the decision-making process by editing the decision function.

Remember to test your modifications thoroughly to ensure the desired results.
